// using System;
// using Assets.Scripts.Controllers;
// using UnityEngine;
// using RPGCharacterAnims.Actions;
//
// namespace RPGCharacterAnims
// {
//     public class GUIControls : MonoBehaviour
//     {
//         private PlayerController _playerController;
//         private RPGCharacterWeaponController rpgCharacterWeaponController;
//         private float charge = 0f;
//         private bool useHips;
//         private bool useDual;
//         private bool useInstant;
//         private bool hipsToggle;
//         private bool dualToggle;
//         private bool instantToggle;
//         private bool crouchToggle;
//         private bool isTalking;
//         private bool isAiming;
//         private bool hipShooting;
//         private bool useNavigation;
//         private float swimTimeout = 0;
//         private Vector3 jumpInput;
//         public GameObject nav;
//
//         private void Start()
//         {
//             // Get other RPG Character components.
//             _playerController = GetComponent<PlayerController>();
//             rpgCharacterWeaponController = GetComponent<RPGCharacterWeaponController>();
//         }
//
//         private void OnGUI()
//         {
//             // Character is not dead.
//             if (!_playerController.isDead) {
//
//                 // Character not climbing or swimming.
//                 if (!_playerController.isClimbing && !_playerController.isSwimming) {
//
//                     // Character is on the ground.
//                     if (_playerController.maintainingGround) {
//                         Blocking();
//
//                         // Character is not Blocking.
//                         if (!_playerController.isBlocking) {
//
// 							// Character is not Casting.
// 							if (!_playerController.isCasting) {
// 								Crouching();
// 								Sprinting();
// 								Charging();
// 								Navigation();
// 								Emotes();
// 								WeaponSwitching();
// 								Attacks();
// 								Damage();
// 								RollDodgeTurn();
// 							}
//                             Casting();
//                         }
//                     }
//                     Jumping();
//                 }
//                 Swimming();
//                 Climbing();
//             }
//             Misc();
//         }
//
//         private void Sprinting()
//         {
// 			// Check to make sure Sprint Action exists.
// 			if (!_playerController.HandlerExists("Sprint")) { return; }
//
// 			if (_playerController.hasNoWeapon) {
// 				bool useSprint = GUI.Toggle(new Rect(640, 115, 100, 30), _playerController.isSprinting, "Sprint");
// 				if (useSprint)
// 					if (_playerController.CanStartAction("Sprint")) { _playerController.StartAction("Sprint"); }
// 				else if (!useSprint) {
// 					if (_playerController.CanEndAction("Sprint")) { _playerController.EndAction("Sprint"); }
// 				}
// 			}
//         }
//
//         private void Charging()
//         {
//             if (_playerController.hasShield) {
//                 GUI.Button(new Rect(620, 140, 100, 30), "Charge");
//                 charge = GUI.HorizontalSlider(new Rect(620, 180, 100, 30), charge, 0.0F, 1f);
//                 _playerController.animator.SetFloat("Charge", charge);
//             }
//         }
//
//         private void Navigation()
//         {
// 			// Check to make sure Navigation Action exists.
//             if (!_playerController.HandlerExists("Navigation")) { return; }
//
//             useNavigation = GUI.Toggle(new Rect(550, 105, 100, 30), useNavigation, "Navigation");
//
//             if (useNavigation) {
//                 nav.transform.GetChild(0).GetComponent<MeshRenderer>().enabled = true;
//                 nav.transform.GetChild(0).GetChild(0).GetComponent<MeshRenderer>().enabled = true;
//                 RaycastHit hit;
//                 if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 100)) {
//                     nav.transform.position = hit.point;
//                     if (Input.GetMouseButtonDown(0)) { _playerController.StartAction("Navigation", hit.point); }
//                 }
//             }
// 			else {
//                 if (_playerController.CanEndAction("Navigation")) {
//                     nav.transform.GetChild(0).GetComponent<MeshRenderer>().enabled = false;
//                     nav.transform.GetChild(0).GetChild(0).GetComponent<MeshRenderer>().enabled = false;
//                     _playerController.EndAction("Navigation");
//                 }
//             }
//         }
//
//         private void Attacks()
//         {
// 			// Check if Attack Action exists.
// 			if (!_playerController.HandlerExists("Attack")) { return; }
//
// 			if (_playerController.CanEndAction("Attack") && _playerController.isSpecial) {
//                 if (GUI.Button(new Rect(235, 85, 100, 30), "End Special")) { _playerController.EndAction("Attack"); }
//             }
//
//             if (!_playerController.CanStartAction("Attack")) { return; }
//
//             if (_playerController.hasLeftWeapon || _playerController.leftWeapon == (int)Weapon.Unarmed) {
//                 if (GUI.Button(new Rect(25, 85, 100, 30), "Attack L")) {
//                     _playerController.StartAction("Attack", new Actions.AttackContext("Attack", "Left"));
//                 }
//             }
//             if (_playerController.hasRightWeapon || _playerController.rightWeapon == (int)Weapon.Unarmed) {
//                 if (GUI.Button(new Rect(130, 85, 100, 30), "Attack R")) {
//                     _playerController.StartAction("Attack", new Actions.AttackContext("Attack", "Right"));
//                 }
//             }
//             if (_playerController.hasTwoHandedWeapon) {
//                 if (GUI.Button(new Rect(130, 85, 100, 30), "Attack")) {
//                     _playerController.StartAction("Attack", new Actions.AttackContext("Attack", "None"));
//                 }
//             }
//             if (_playerController.hasDualWeapons) {
//
//                 // Can't Dual Attack with Item weapons.
//                 if (_playerController.rightWeapon != 14 && _playerController.leftWeapon != 15) {
//                     if (GUI.Button(new Rect(235, 85, 100, 30), "Attack Dual")) {
//                         _playerController.StartAction("Attack", new Actions.AttackContext("Attack", "Dual"));
//                     }
//                 }
//             }
//             //Special Attack.
//             if (_playerController.hasTwoHandedWeapon) {
//                 if (GUI.Button(new Rect(335, 85, 100, 30), "Special Attack1")) {
//                     _playerController.StartAction("Attack", new Actions.AttackContext("Special", "None"));
//                 }
//             }
//             // Sword + Mace Special Attack.
//             if ((_playerController.leftWeapon == 8 || _playerController.leftWeapon == 10)
//                 && (_playerController.rightWeapon == 9 || _playerController.rightWeapon == 11)) {
//                 if (GUI.Button(new Rect(335, 85, 100, 30), "Special Attack1")) {
//                     _playerController.StartAction("Attack", new Actions.AttackContext("Special", "Right"));
//                 }
//             }
//             // Kicking.
//             if (GUI.Button(new Rect(25, 115, 100, 30), "Left Kick")) { _playerController.StartAction("Attack", new Actions.AttackContext("Kick", "Left", 1)); }
//             if (GUI.Button(new Rect(25, 145, 100, 30), "Left Kick2")) { _playerController.StartAction("Attack", new Actions.AttackContext("Kick", "Left", 2)); }
//             if (GUI.Button(new Rect(130, 115, 100, 30), "Right Kick")) { _playerController.StartAction("Attack", new Actions.AttackContext("Kick", "Right", 3)); }
//             if (GUI.Button(new Rect(130, 145, 100, 30), "Right Kick2")) { _playerController.StartAction("Attack", new Actions.AttackContext("Kick", "Right", 4)); }
//         }
//
//         private void Damage()
//         {
// 			// Check if Get Hit Action exists.
// 			if (_playerController.HandlerExists("GetHit")) {
// 				if (GUI.Button(new Rect(30, 240, 100, 30), "Get Hit")) { _playerController.StartAction("GetHit", new HitContext()); }
// 			}
// 			// Check if Knockback Action exists.
// 			if (_playerController.HandlerExists("Knockback")) {
// 				if (GUI.Button(new Rect(130, 240, 100, 30), "Knockback1")) { _playerController.StartAction("Knockback", new HitContext(1, Vector3.back)); }
// 				if (GUI.Button(new Rect(230, 240, 100, 30), "Knockback2")) { _playerController.StartAction("Knockback", new HitContext(2, Vector3.back)); }
// 			}
// 			// Check if Knockdown Action exists.
// 			if (_playerController.HandlerExists("Knockdown")) {
// 				if (GUI.Button(new Rect(130, 270, 100, 30), "Knockdown")) { _playerController.StartAction("Knockdown", new HitContext(1, Vector3.back)); }
// 			}
//         }
//
//         private void Crouching()
//         {
// 			// Check if Crouch Action exists.
// 			if (_playerController.HandlerExists("Crouch")) {
// 				bool useCrouch = GUI.Toggle(new Rect(640, 95, 100, 30), _playerController.isCrouching, "Crouch");
// 				if (useCrouch && _playerController.CanStartAction("Crouch")) { _playerController.StartAction("Crouch"); } else if (!useCrouch && _playerController.CanEndAction("Crouch")) { _playerController.EndAction("Crouch"); }
//
// 				// Check if Crawl Action exists.
// 				if (!_playerController.HandlerExists("Crawl")) { return; }
//
// 				bool useCrawl = _playerController.isCrawling;
// 				if (useCrouch) {
// 					if (GUI.Button(new Rect(640, 140, 100, 30), "Crawl")) {
// 						_playerController.Crawl();
// 						if (_playerController.CanStartAction("Crawl")) { _playerController.StartAction("Crawl"); }
// 						if (_playerController.CanEndAction("Crouch")) { _playerController.EndAction("Crouch"); }
// 					}
// 				}
// 				if (useCrawl) {
// 					if (GUI.Button(new Rect(640, 140, 100, 30), "Crawl")) {
// 						_playerController.StartAction("Idle");
// 						if (_playerController.CanStartAction("Crouch")) { _playerController.StartAction("Crouch"); }
// 					}
// 				}
// 			}
//         }
//
//         private void Blocking()
//         {
// 			// Check if Block Action exists.
// 			if (!_playerController.HandlerExists("Block")) { return; }
//
// 			if (!_playerController.isCasting && !_playerController.isSitting && !_playerController.IsActive("Relax")) {
//                 bool blockGui = GUI.Toggle(new Rect(25, 215, 100, 30), _playerController.isBlocking, "Block");
//
//                 if (blockGui && _playerController.CanStartAction("Block")) { _playerController.StartAction("Block"); }
// 				else if (!blockGui && _playerController.CanEndAction("Block")) { _playerController.EndAction("Block"); }
//
//                 if (blockGui) {
//
// 					// Check if Get Hit Action exists.
// 					if (!_playerController.HandlerExists("Get Hit")) { return; }
//
// 					if (GUI.Button(new Rect(30, 240, 100, 30), "Get Hit")) { _playerController.StartAction("GetHit", new HitContext()); }
//                 }
//             }
//         }
//
// 		private void RollDodgeTurn()
// 		{
// 			// Check if Roll Action exists.
// 			if (_playerController.HandlerExists("Block")) {
// 				if (_playerController.CanStartAction("Roll")) {
// 					if (GUI.Button(new Rect(25, 15, 100, 30), "Roll Forward")) { _playerController.StartAction("Roll", 1); }
// 					if (GUI.Button(new Rect(130, 15, 100, 30), "Roll Backward")) { _playerController.StartAction("Roll", 3); }
// 					if (GUI.Button(new Rect(25, 45, 100, 30), "Roll Left")) { _playerController.StartAction("Roll", 4); }
// 					if (GUI.Button(new Rect(130, 45, 100, 30), "Roll Right")) { _playerController.StartAction("Roll", 2); }
// 				}
// 			}
// 			// Check if Dodge Action exists.
// 			if (_playerController.HandlerExists("Dodge")) {
// 				if (_playerController.CanStartAction("Dodge")) {
// 					if (GUI.Button(new Rect(235, 15, 100, 30), "Dodge Left")) { _playerController.StartAction("Dodge", 1); }
// 					if (GUI.Button(new Rect(235, 45, 100, 30), "Dodge Right")) { _playerController.StartAction("Dodge", 2); }
// 				}
// 			}
// 			// Check if Turn Action exists.
// 			if (_playerController.HandlerExists("Turn")) {
// 				if (_playerController.CanStartAction("Turn")) {
// 					if (GUI.Button(new Rect(340, 15, 100, 30), "Turn Left")) { _playerController.StartAction("Turn", 1); }
// 					if (GUI.Button(new Rect(340, 45, 100, 30), "Turn Right")) { _playerController.StartAction("Turn", 2); }
// 					if (GUI.Button(new Rect(445, 15, 100, 30), "Turn Left 180")) { _playerController.StartAction("Turn", 3); }
// 					if (GUI.Button(new Rect(445, 45, 100, 30), "Turn Right 180")) { _playerController.StartAction("Turn", 4); }
// 				}
// 			}
// 			// Check if DiveRoll Action exists.
// 			if (_playerController.HandlerExists("DiveRoll")) {
// 				if (_playerController.CanStartAction("DiveRoll")) {
// 					if (GUI.Button(new Rect(445, 75, 100, 30), "Dive Roll")) { _playerController.StartAction("DiveRoll", 1); }
// 				}
// 			}
// 		}
//
//         private void Casting()
//         {
// 			// Check if Cast Action exists.
// 			if (!_playerController.HandlerExists("Cast")) { return; }
//
// 			if (_playerController.CanEndAction("Cast") && _playerController.isCasting) {
//                 if (GUI.Button(new Rect(25, 330, 100, 30), "Stop Casting")) { _playerController.EndAction("Cast"); }
//             }
//
//             if (!_playerController.CanStartAction("Cast")) { return; }
//
//             bool leftUnarmed = _playerController.leftWeapon == (int)Weapon.Unarmed;
//             bool rightUnarmed = _playerController.rightWeapon == (int)Weapon.Unarmed;
//             bool wieldStaff = _playerController.rightWeapon == (int)Weapon.TwoHandStaff;
//
//             if (leftUnarmed && GUI.Button(new Rect(25, 330, 100, 30), "Cast Atk Left")) {
//                 _playerController.StartAction("Cast", new Actions.CastContext("Cast", "Left"));
//             }
//             if (rightUnarmed && GUI.Button(new Rect(125, 330, 100, 30), "Cast Atk Right")) {
//                 _playerController.StartAction("Cast", new Actions.CastContext("Cast", "Right"));
//             }
//             if (leftUnarmed && rightUnarmed && GUI.Button(new Rect(80, 365, 100, 30), "Cast Atk Dual")) {
//                 _playerController.StartAction("Cast", new Actions.CastContext("Cast", "Dual"));
//             }
//             if (!_playerController.hasDualWeapons
// 				&& (AnimationData.IsCastableWeapon(_playerController.leftWeapon) || AnimationData.IsCastableWeapon(_playerController.rightWeapon))) {
//                 if (GUI.Button(new Rect(25, 425, 100, 30), "Cast AOE")) { _playerController.StartAction("Cast", new Actions.CastContext("AOE", "Dual")); }
//                 if (GUI.Button(new Rect(25, 400, 100, 30), "Cast Buff")) { _playerController.StartAction("Cast", new Actions.CastContext("Buff", "Dual")); }
//                 if (GUI.Button(new Rect(25, 450, 100, 30), "Cast Summon")) { _playerController.StartAction("Cast", new Actions.CastContext("Summon", "Dual")); }
//             }
//         }
//
//         private void Jumping()
//         {
// 			// Check if Jump Action exists.
// 			if (!_playerController.HandlerExists("Jump")) { return; }
//
// 			if (_playerController.CanStartAction("Jump")) {
//                 if (GUI.Button(new Rect(25, 175, 100, 30), "Jump")) {
//                     _playerController.SetJumpInput(Vector3.up);
//                     _playerController.StartAction("Jump");
//                 }
//             }
//             if (_playerController.CanStartAction("DoubleJump")) {
//                 if (GUI.Button(new Rect(25, 175, 100, 30), "Jump Flip")) {
//                     _playerController.SetJumpInput(Vector3.up);
//                     _playerController.StartAction("DoubleJump");
//                 }
//             }
//         }
//
//         private void Emotes()
//         {
// 			// Check if Emote Action exists.
// 			if (_playerController.HandlerExists("Emote")) {
// 				if (_playerController.CanStartAction("Emote")) {
// 					string emote = "";
// 					if (GUI.Button(new Rect(665, 680, 100, 30), "Sleep")) { emote = "Sleep"; }
// 					if (GUI.Button(new Rect(770, 680, 100, 30), "Sit")) { emote = "Sit"; }
// 					if (GUI.Button(new Rect(770, 650, 100, 30), "Drink")) { emote = "Drink"; }
// 					if (GUI.Button(new Rect(665, 650, 100, 30), "Bow")) { emote = "Bow"; }
// 					if (GUI.Button(new Rect(560, 650, 100, 30), "Yes")) { emote = "Yes"; }
// 					if (GUI.Button(new Rect(455, 650, 100, 30), "No")) { emote = "No"; }
// 					if (GUI.Button(new Rect(560, 680, 100, 30), "Start Talking")) { emote = "Talk"; }
//
// 					if (emote != "") { _playerController.StartAction("Emote", emote); }
// 				}
// 				if (_playerController.CanEndAction("Emote")) {
// 					if (_playerController.isSitting) {
// 						if (GUI.Button(new Rect(795, 680, 100, 30), "Stand")) { _playerController.EndAction("Emote"); }
// 					}
// 					if (_playerController.isTalking) {
// 						if (GUI.Button(new Rect(795, 680, 100, 30), "Stop Talking")) { _playerController.EndAction("Emote"); }
// 					}
// 				}
// 			}
//
// 			// Check if Emote Combat Action exists.
// 			if (_playerController.HandlerExists("EmoteCombat")) {
// 				if (_playerController.CanStartAction("EmoteCombat")) {
// 					string emote = "";
// 					if (GUI.Button(new Rect(130, 175, 100, 30), "Pickup")) { emote = "Pickup"; }
// 					if (GUI.Button(new Rect(235, 175, 100, 30), "Activate")) { emote = "Activate"; }
// 					if (GUI.Button(new Rect(480, 650, 100, 30), "Boost")) { emote = "Boost"; }
//
// 					if (emote != "") { _playerController.StartAction("EmoteCombat", emote); }
// 				}
// 			}
//         }
//
//         private void Climbing()
//         {
// 			// Check if Climb Ladder  Action exists.
// 			if (_playerController.HandlerExists("ClimbLadder")) {
// 				if (_playerController.CanStartAction("ClimbLadder")) {
// 					if (GUI.Button(new Rect(640, 360, 100, 30), "Climb Ladder")) { _playerController.StartAction("ClimbLadder"); }
// 				}
// 			}
//         }
//
//         private void Swimming()
//         {
//             if (_playerController.isSwimming) {
//                 float swimTime = 0.5f;
//
//                 if (GUI.Button(new Rect(25, 175, 100, 30), "Swim Up")) {
//                     swimTimeout = Time.time + swimTime;
//                     jumpInput = Vector3.up;
//
//                     // Override the jump input for a half second to simulate a button press.
//                     RPGCharacterInputController inputController = _playerController.GetComponent<RPGCharacterInputController>();
//                     if (inputController != null) { inputController.PauseInput(swimTime); }
//                 }
//                 if (GUI.Button(new Rect(25, 225, 100, 30), "Swim Down")) {
//                     swimTimeout = Time.time + swimTime;
//                     jumpInput = Vector3.down;
//
//                     // Override the jump input for a half second to simulate a button press.
//                     RPGCharacterInputController inputController = _playerController.GetComponent<RPGCharacterInputController>();
//                     if (inputController != null) { inputController.PauseInput(swimTime); }
//                 }
//
//                 if (Time.time < swimTimeout) { _playerController.SetJumpInput(jumpInput); }
//             }
//         }
//
//         // Death / Debug.
//         private void Misc()
//         {
//             string deathReviveLabel = _playerController.isDead ? "Revive" : "Death";
//             if (!_playerController.isClimbing && !_playerController.isCasting && !_playerController.isSitting && _playerController.maintainingGround) {
//
// 				// Check if Climb Ladder  Action exists.
// 				if (_playerController.HandlerExists("Death")) {
// 					if (GUI.Button(new Rect(30, 270, 100, 30), deathReviveLabel)) {
// 						if (_playerController.CanStartAction("Death")) { _playerController.StartAction("Death"); } else if (_playerController.CanEndAction("Death")) { _playerController.EndAction("Death"); }
// 					}
// 				}
//             }
//             // Debug.
//             if (GUI.Button(new Rect(600, 20, 120, 30), "Debug Controller")) { _playerController.ControllerDebug(); }
//             if (GUI.Button(new Rect(600, 50, 120, 30), "Debug Animator")) { _playerController.AnimatorDebug(); }
//         }
//
//
// 		private void WeaponSwitching()
// 		{
// 			// Check if SwitchWeapon Action exists.
// 			if (!_playerController.HandlerExists("SwitchWeapon")) { return; }
//
// 			bool doSwitch = false;
// 			SwitchWeaponContext context = new SwitchWeaponContext();
//
// 			// Check if Relax Action exists.
// 			if (_playerController.HandlerExists("Relax")) {
// 				if (!_playerController.isRelaxed) {
// 					if (GUI.Button(new Rect(1115, 240, 100, 30), "Relax")) {
// 						if (useInstant) { _playerController.StartAction("Relax", true); }
// 						else { _playerController.StartAction("Relax"); }
// 					}
// 				}
// 			}
// 			if (_playerController.rightWeapon != ( int )Weapon.Unarmed || _playerController.leftWeapon != ( int )Weapon.Unarmed) {
// 				if (GUI.Button(new Rect(1115, 280, 100, 30), "Unarmed")) {
// 					doSwitch = true;
// 					context.type = "Switch";
// 					context.side = "Dual";
// 					context.leftWeapon = ( int )Weapon.Unarmed;
// 					context.rightWeapon = ( int )Weapon.Unarmed;
// 				}
// 			}
//
// 			// Two-handed weapons.
// 			Weapon[] weapons = new Weapon[] {
// 				Weapon.TwoHandSword,
// 				Weapon.TwoHandSpear,
// 				Weapon.TwoHandAxe,
// 				Weapon.TwoHandStaff,
// 				Weapon.TwoHandBow,
// 				Weapon.TwoHandCrossbow,
// 				Weapon.Rifle,
// 			};
//
// 			int offset = 310;
//
// 			foreach (Weapon weapon in weapons) {
// 				if (_playerController.rightWeapon != ( int )weapon) {
// 					string label = weapon.ToString();
// 					if (label.StartsWith("TwoHand")) { label = label.Replace("TwoHand", "2H "); }
// 					if (GUI.Button(new Rect(1115, offset, 100, 30), label)) {
// 						doSwitch = true;
// 						context.type = "Switch";
// 						context.side = "None";
// 						context.leftWeapon = -1;
// 						context.rightWeapon = ( int )weapon;
// 					}
// 				}
// 				offset += 30;
// 			}
//
// 			// Left/Right weapon pairs.
// 			Tuple<Weapon, Weapon>[] leftRightPairs = new Tuple<Weapon, Weapon>[] {
// 				Tuple.Create(Weapon.LeftSword, Weapon.RightSword),
// 				Tuple.Create(Weapon.LeftMace, Weapon.RightMace),
// 				Tuple.Create(Weapon.LeftDagger, Weapon.RightDagger),
// 				Tuple.Create(Weapon.LeftItem, Weapon.RightItem),
// 				Tuple.Create(Weapon.LeftPistol, Weapon.RightPistol),
// 				Tuple.Create(Weapon.Shield, Weapon.RightSpear),
// 			};
//
// 			offset = 530;
//
// 			foreach (Tuple<Weapon, Weapon> pair in leftRightPairs) {
// 				bool missingOneSide = false;
//
// 				// Left weapons.
// 				if (_playerController.leftWeapon != ( int )pair.Item1) {
// 					missingOneSide = true;
// 					if (GUI.Button(new Rect(1065, offset, 100, 30), pair.Item1.ToString())) {
// 						doSwitch = true;
// 						context.type = "Switch";
// 						context.side = "Left";
// 						context.leftWeapon = ( int )pair.Item1;
// 						context.rightWeapon = -1;
// 					}
// 				}
// 				// Right weapons.
// 				if (_playerController.rightWeapon != ( int )pair.Item2) {
// 					missingOneSide = true;
// 					if (GUI.Button(new Rect(1165, offset, 100, 30), pair.Item2.ToString())) {
// 						doSwitch = true;
// 						context.type = "Switch";
// 						context.side = "Right";
// 						context.leftWeapon = -1;
// 						context.rightWeapon = ( int )pair.Item2;
// 					}
// 				}
// 				// If at least one side isn't carrying this weapon, show the Dual switch.
// 				if (missingOneSide) {
// 					string label = pair.Item1.ToString();
// 					if (!label.Contains("Shield")) {
// 						label = label.Replace("Left", "Dual ") + "s";
// 						if (GUI.Button(new Rect(965, offset, 100, 30), label)) {
// 							doSwitch = true;
// 							context.type = "Switch";
// 							context.side = "Dual";
// 							context.leftWeapon = ( int )pair.Item1;
// 							context.rightWeapon = ( int )pair.Item2;
// 						}
// 					}
// 				}
//
// 				offset += 30;
// 			}
// 			if (_playerController.leftWeapon > ( int )Weapon.Unarmed) {
// 				if (GUI.Button(new Rect(750, offset - 150, 100, 30), "Sheath Left")) {
// 					doSwitch = true;
// 					context.type = "Sheath";
// 					context.side = "Left";
// 					context.leftWeapon = ( int )Weapon.Unarmed;
// 					context.rightWeapon = -1;
// 				}
// 			}
// 			if (_playerController.rightWeapon > ( int )Weapon.Unarmed) {
// 				if (GUI.Button(new Rect(850, offset - 150, 100, 30), "Sheath Right")) {
// 					doSwitch = true;
// 					context.type = "Sheath";
// 					context.side = "Right";
// 					context.leftWeapon = -1;
// 					context.rightWeapon = ( int )Weapon.Unarmed;
// 				}
// 			}
//
// 			offset += 30;
//
// 			if (GUI.Button(new Rect(965, 680, 100, 30), "Shuffle")) {
// 				int lefty = ( int )leftRightPairs[UnityEngine.Random.Range(0, leftRightPairs.Length)].Item1;
// 				int righty = ( int )leftRightPairs[UnityEngine.Random.Range(0, leftRightPairs.Length)].Item2;
//
// 				doSwitch = true;
// 				context.type = "Switch";
// 				context.side = "Both";
// 				context.leftWeapon = lefty;
// 				context.rightWeapon = righty;
// 			}
// 			// Check if HipShoot Action exists.
// 			if (_playerController.HandlerExists("HipShoot")) {
// 				hipShooting = GUI.Toggle(new Rect(1000, 495, 100, 30), hipShooting, "Hip Shooting");
// 				if (hipShooting) {
// 					if (_playerController.CanStartAction("HipShoot")) { _playerController.StartAction("HipShoot"); }
// 					else {
// 						if (_playerController.CanEndAction("HipShoot")) { _playerController.EndAction("HipShoot"); }
// 					}
// 				}
// 			}
//
// 			// Sheath/Unsheath Hips.
// 			useHips = GUI.Toggle(new Rect(1000, 260, 100, 30), useHips, "Hips");
// 			if (useHips) { context.sheathLocation = "Hips"; } else { context.sheathLocation = "Back"; }
//
// 			// Instant weapon toggle.
// 			useInstant = GUI.Toggle(new Rect(1000, 310, 100, 30), useInstant, "Instant");
// 			if (useInstant) { context.type = "Instant"; }
//
// 			// Perform the weapon switch.
// 			if (doSwitch) {
// 				if (_playerController.CanStartAction("SwitchWeapon")) { _playerController.StartAction("SwitchWeapon", context); }
// 			}
// 		}
// 	}
// }