﻿// using UnityEngine;
//
// namespace RPGCharacterAnims
// {
//     public class HighJumpTrampoline : MonoBehaviour
//     {
//         GameObject character;
//         float oldJumpSpeed;
//
//         void Update()
//         {
//             if (character != null) {
//                 PlayerController controller = character.GetComponent<PlayerController>();
//                 controller.SetJumpInput(Vector3.up);
//                 if (controller.CanStartAction("Jump")) {
//                     controller.StartAction("Jump");
//                 }
//             }
//         }
//
//         private void OnTriggerEnter(Collider collide)
//         {
//             PlayerController controller = collide.gameObject.GetComponent<PlayerController>();
//
//             if (controller != null) {
//                 character = collide.gameObject;
//
//                 PlayerController movement = character.GetComponent<PlayerController>();
//                 oldJumpSpeed = movement.jumpSpeed;
//                 movement.jumpSpeed = oldJumpSpeed * 2f;
// 				Debug.Log("Trampoline!");
// 			}
//         }
//
//         private void OnTriggerExit(Collider collide)
//         {
//             if (collide.gameObject == character) {
//                 PlayerController movement = character.GetComponent<PlayerController>();
//                 movement.jumpSpeed = oldJumpSpeed;
//                 character = null;
//             }
//         }
//     }
// }