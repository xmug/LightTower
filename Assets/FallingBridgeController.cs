using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingBridgeController : MonoBehaviour
{
    private void OnTriggerExit(Collider other)
    {
        if (other.TryGetComponent<FallingBridgeHolder>(out var holder))
        {
            var rb = GetComponent<Rigidbody>();
            rb.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ |
                             RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
        }
    }
}