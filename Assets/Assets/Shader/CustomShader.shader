Shader "Toon/Lit Distance Appear" {
    Properties{
        _Color("Color Primary", Color) = (0.5,0.5,0.5,1)        
        _MainTex("Main Texture", 2D) = "white" {}
        _Speed("MoveSpeed", Range(1,50)) = 10 // speed of the swaying
        [Toggle(DOWN_ON)] _DOWN("Move Down?", Int) = 0
            
    }
 
        SubShader{
            Tags { "RenderType" = "Opaque"  }
            LOD 200
 
        CGPROGRAM
        #pragma surface surf ToonRamp vertex:vert addshadow // addshadow applies shadow after vertex animation
 
        // custom lighting function based
        // on angle between light direction and normal
        #pragma lighting ToonRamp exclude_path:prepass
        #pragma multi_compile_instancing
         #pragma shader_feature DOWN_ON

 
        sampler2D _MainTex;
        float _Speed;
        float4 _Color;
 
        struct Input {
            float2 uv_MainTex : TEXCOORD0;
        };
 
        UNITY_INSTANCING_BUFFER_START(Props)
        UNITY_DEFINE_INSTANCED_PROP(float, _Moved)
        UNITY_INSTANCING_BUFFER_END(Props)
 
        void vert(inout appdata_full v)//
        {
            v.vertex.xyz *= UNITY_ACCESS_INSTANCED_PROP(Props, _Moved);
            #if DOWN_ON
            v.vertex.y += _Speed-UNITY_ACCESS_INSTANCED_PROP(Props, _Moved * _Speed);
            #else
            v.vertex.y -= _Speed-UNITY_ACCESS_INSTANCED_PROP(Props, _Moved * _Speed);
            #endif
        }

        ENDCG
        }
           
}