﻿using System;
using UnityEngine;

namespace Assets.Scripts.Utility
{
    public class DynamicMeshCollider : MonoBehaviour
    {
        private MeshCollider collider;
        private SkinnedMeshRenderer _meshRenderer;
        
        private void Awake()
        { 
            collider = GetComponent<MeshCollider>();
            _meshRenderer = GetComponent<SkinnedMeshRenderer>();
        }

        private void FixedUpdate()
        {
            Mesh colliderMesh = new Mesh();
            _meshRenderer.BakeMesh(colliderMesh); //更新mesh
            Destroy(collider.sharedMesh);
            collider.sharedMesh = null;
            collider.sharedMesh = colliderMesh; //将新的mesh赋给meshcollider
        }
    }
}