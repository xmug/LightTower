using Assets.Scripts.Models;
using XMUGFramework;
using XMUGFramework.Command;

public class GameCore : BaseCore<GameCore>
{
    protected override void Init()
    {
        Register(new PlayerModel());
        Register(new GameModel());
        Register(new EventSystem());
        Register(new PlayerPrefsStorage());
    }
}
