using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Events;
using Assets.Scripts.Models;
using JetBrains.Annotations;
using RPGCharacterAnims;
using RPGCharacterAnims.Actions;
using UnityEngine;
using XMUGFramework;
using Random = UnityEngine.Random;

namespace Assets.Scripts.Controllers
{
    public enum PlayerState
    {
        Idle = 0,
        Move = 1,
        Jump = 2,
        DoubleJump = 3,
        Fall = 4,
        Roll = 8,
        DiveRoll = 11,
        PushPull = 12
    }

    [RequireComponent(typeof(SuperCharacterController))]
    [RequireComponent(typeof(CapsuleCollider))]
    [RequireComponent(typeof(Rigidbody))]
    public class PlayerController : SuperStateMachine
    {
        [HideInInspector] public Animator animator;
        private SuperCharacterController superCharacterController;
        private CapsuleCollider capCollider;
        private Rigidbody rb;

        public event System.Action OnLockMovement = delegate { };
        public event System.Action OnUnlockMovement = delegate { };

        private PlayerState state;
        public float animationSpeed = 1;
        public bool idleAlert = true;

        [Header("LightRelated")] public float warmUpSpeed = 3f;
        public float fuelUsingSpeed = 0.5f;

        [Header("Movement")] public float runSpeed = 1f;
        public float runAccel = 30f;
        public float sprintSpeed = 2.5f;
        public float sprintAccel = 15;
        public float pushPullSpeed = 0.15f;
        public float pushPullAccel = 15;
        public float groundFriction = 120f;
        public float dashForce = 10f;
        public float rotationSpeed = 100f;
        private PushPullHandle pushPullHandle;

        [Header("Jumping")] public float jumpSpeed = 12f;
        public float jumpGravity = 24f;
        public float doubleJumpSpeed = 8f;
        public float inAirSpeed = 8f;
        public float inAirAccel = 16f;
        public float fallGravity = 32f;
        public bool fallingControl = false;

        [HideInInspector] public bool canJump;
        [HideInInspector] public bool holdingJump;
        [HideInInspector] public bool canDoubleJump = false;
        private bool doublejumped = false;

        [Header("Knockback")] public float knockbackMultiplier = 1f;

        [Header("Movement Multiplier")] public float movementAnimationMultiplier = 1f;


        // inputs
        public Vector3 MoveInput => _moveInput;
        private Vector3 _moveInput;

        public Vector3 jumpInput => _jumpInput;
        private Vector3 _jumpInput;

        public Vector3 faceInput => _faceInput;
        private Vector3 _faceInput;

        public Vector3 cameraRelativeInput => _cameraRelativeInput;
        private Vector3 _cameraRelativeInput;


        // State Conditions
        public bool canFace => _canFace && !isDead && !isDead;
        private bool _canFace = true;

        public bool canStrafe => _canStrafe && !isDead;
        private bool _canStrafe = true;

        public bool canMove => _canMove && !isDead;
        private bool _canMove;

        public bool canAction => _canAction && !isDead;
        private bool _canAction;

        // ComplexState

        # region special state info

        public bool acquiringGround
        {
            get
            {
                if (HandlerExists("AcquiringGround"))
                {
                    return IsActive("AcquiringGround");
                }

                return false;
            }
        }


        public bool isAiming
        {
            get
            {
                if (HandlerExists("Aim"))
                {
                    return IsActive("Aim");
                }

                return false;
            }
        }


        public bool isDead
        {
            get
            {
                if (HandlerExists("Death"))
                {
                    return IsActive("Death");
                }

                return false;
            }
        }

        public bool isFacing
        {
            get
            {
                if (HandlerExists("Face"))
                {
                    return IsActive("Face");
                }

                return false;
            }
        }

        public bool isFalling
        {
            get
            {
                if (HandlerExists("Fall"))
                {
                    return IsActive("Fall");
                }

                return false;
            }
        }


        public bool isIdle
        {
            get
            {
                if (HandlerExists("Idle"))
                {
                    return IsActive("Idle");
                }

                return false;
            }
        }

        public bool isMoving
        {
            get
            {
                if (HandlerExists("Move"))
                {
                    return IsActive("Move");
                }

                return false;
            }
        }

        public bool isRelaxed
        {
            get
            {
                if (HandlerExists("Relax"))
                {
                    return IsActive("Relax");
                }

                return false;
            }
        }

        public bool isSprinting
        {
            get
            {
                if (HandlerExists("Sprint"))
                {
                    return IsActive("Sprint");
                }

                return false;
            }
        }

        public bool isBeamRotating { get; set; }

        public bool isPushPulling
        {
            get
            {
                if (HandlerExists("PushPull"))
                {
                    return IsActive("PushPull");
                }

                return false;
            }
        }

        public bool maintainingGround
        {
            get
            {
                if (HandlerExists("MaintainingGround"))
                {
                    return IsActive("MaintainingGround");
                }

                return false;
            }
        }

        #endregion

        #region outside

        public PlayerModel playerModel;
        public EventSystem eSys;

        #endregion

        // showDebug log
        [SerializeField] private bool showDebug = false;

        // Action Pool
        private Dictionary<string, IActionHandler> actionHandlers = new Dictionary<string, IActionHandler>();

        private void Awake()
        {
            pushPullHandle = GetComponentInChildren<PushPullHandle>();
            // Setup Animator, add AnimationEvents script.
            animator = GetComponentInChildren<Animator>();

            if (animator == null)
            {
                Debug.LogError("ERROR: THERE IS NO ANIMATOR COMPONENT ON CHILD OF CHARACTER.");
                Debug.Break();
            }

            animator.gameObject.AddComponent<RPGCharacterAnimatorEvents>();
            animator.updateMode = AnimatorUpdateMode.AnimatePhysics;
            animator.cullingMode = AnimatorCullingMode.CullUpdateTransforms;
            animator.SetInteger("Weapon", 0);
            animator.SetInteger("WeaponSwitch", 0);

            SetHandler("Attack", new Attack());
            SetHandler("Death", new SimpleActionHandler(Death, Revive));
            SetHandler("Face", new SimpleActionHandler(StartFace, EndFace));
            SetHandler("Injure", new SimpleActionHandler(StartInjured, EndInjured));
            SetHandler("Null", new Null());
            SetHandler("SlowTime", new SlowTime());
            SetHandler("Sprint", new SimpleActionHandler(StartSprint, EndSprint));
            SetHandler("Strafe", new SimpleActionHandler(StartStrafe, EndStrafe));
            SetHandler("EmoteCombat", new EmoteCombat());
            SetHandler("AcquiringGround", new SimpleActionHandler(() => { }, () => { }));
            SetHandler("MaintainingGround", new SimpleActionHandler(() => { }, () => { }));
            SetHandler("DiveRoll", new DiveRoll(this));
            SetHandler("DoubleJump", new DoubleJump(this));
            SetHandler("Fall", new Fall(this));
            SetHandler("GetHit", new GetHit(this));
            SetHandler("Idle", new Idle(this));
            SetHandler("Jump", new Jump(this));
            SetHandler("Move", new Move(this));
            SetHandler("Roll", new Roll(this));
            SetHandler("PushPull", new SimpleActionHandler(StartPushPull, EndPushPull));
            SetHandler("BeamRotating", new SimpleActionHandler(StartBeamRotating, EndBeamRotating));

            // Unlock actions and movement.
            Unlock(true, true);

            eSys = GameCore.Get<EventSystem>();
            playerModel = GameCore.Get<PlayerModel>();

            playerModel.playerController = this;
            playerModel.warmth.OnValueChanged += f =>
            {
                if (f <= 0)
                {
                    if (HandlerExists("Death"))
                    {
                        if (CanStartAction("Death"))
                        {
                            StartAction("Death");
                            eSys.Trigger(new PlayerDeathEvent());
                        }
                    }
                }
            };

            eSys.Register<PlayerGetHitEvent>(param =>
            {
                if (HandlerExists("GetHit"))
                {
                    StartAction("GetHit", new HitContext());
                }
            });

            eSys.Register<PlayerReviveEvent>(param =>
            {
                transform.position = GameCore.Get<GameModel>().savePointPos.Value;
                if (HandlerExists("Death"))
                {
                    if (CanEndAction("Death"))
                    {
                        EndAction("Death");
                    }
                }
            });

            eSys.Register<ObjectEvents.ObjectActivateCallBack>(param =>
            {
                switch (param.interactableObject)
                {
                    case LightBeamer beamer:
                        if (!HandlerExists("BeamRotating"))
                        {
                            return;
                        }

                        if (param.toActivate)
                        {
                            StartAction("BeamRotating");
                            isBeamRotating = true;
                            beamer.transform.SetParent(transform.parent);
                            transform.position = beamer.handle.transform.position;
                            transform.rotation = beamer.handle.transform.rotation;
                            transform.SetParent(beamer.transform);
                            playerModel.currentVelocity.Value = Vector3.zero;
                            animator.SetFloat("Velocity X", 0f);
                            animator.SetFloat("Velocity Z", 0f);
                            animator.SetBool("Moving", false);
                        }
                        else
                        {
                            EndAction("BeamRotating");
                            isBeamRotating = false;
                            transform.SetParent(beamer.transform.parent);
                            transform.position = beamer.handle.transform.position;
                        }

                        break;
                    case ObjectPushPull objectPushPull:
                        if (!HandlerExists("PushPull"))
                        {
                            return;
                        }

                        if (param.toActivate)
                        {
                            if (CanStartAction("PushPull"))
                            {
                                StartAction("PushPull");
                                objectPushPull.transform.SetParent(pushPullHandle.transform);
                                var obj = pushPullHandle.transform.position +
                                          pushPullHandle.transform.forward *
                                          objectPushPull.transform.localScale.z / 2;
                                objectPushPull.transform.position = new Vector3(obj.x,
                                    (pushPullHandle.transform.position +
                                    pushPullHandle.transform.up *
                                    objectPushPull.transform.localScale.y / 2).y, obj.z);
                                objectPushPull.transform.rotation = pushPullHandle.transform.rotation;
                            }
                        }
                        else
                        {
                            if (CanEndAction("PushPull"))
                            {
                                EndAction("PushPull");
                                pushPullHandle.transform.DetachChildren();
                            }
                        }

                        break;
                    case BonfireController bonfire:
                        if (param.toActivate)
                        {
                            if (HandlerExists("EmoteCombat"))
                            {
                                if (CanStartAction("EmoteCombat"))
                                {
                                    StartAction("EmoteCombat", "Activate");
                                }
                            }
                        }

                        break;
                    default:
                        Debug.LogWarning("Currently nothing interactable arround");
                        break;
                }
            });
            
            eSys.Register<PlayerInputLockEvent>(param =>
            {
                Lock(true, true, true, 0, param.time);
            });
        }

        private void Start()
        {
            // Get other RPG Character components.
            superCharacterController = GetComponent<SuperCharacterController>();

            // Check if Animator exists, otherwise pause script.
            animator = GetComponentInChildren<Animator>();
            if (animator == null)
            {
                Debug.LogError("ERROR: THERE IS NO ANIMATOR COMPONENT ON CHILD OF CHARACTER.");
                Debug.Break();
            }

            // Setup Collider and Rigidbody for collisions.
            capCollider = GetComponent<CapsuleCollider>();
            rb = GetComponent<Rigidbody>();

            // Set restraints on startup if using Rigidbody.
            if (rb != null)
            {
                rb.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
            }

            OnLockMovement += LockMovement;
            OnUnlockMovement += UnlockMovement;

            RPGCharacterAnimatorEvents animatorEvents =
                animator.gameObject.GetComponent<RPGCharacterAnimatorEvents>();

            animatorEvents.OnMove.AddListener(AnimatorMove);
        }

        #region Updates

        void Update()
        {
            if (!superCharacterController.enabled)
            {
                gameObject.SendMessage("SuperUpdate", SendMessageOptions.DontRequireReceiver);
            }
        }

        private void FixedUpdate()
        {
            playerModel.torchFuel.Value =
                Mathf.Clamp((playerModel.torchFuel.Value - fuelUsingSpeed * Time.fixedDeltaTime), 0f, 100f);
            if (playerModel.torchFuel.Value > 0f)
            {
                playerModel.warmth.Value = Mathf.Clamp((playerModel.warmth.Value + warmUpSpeed * Time.fixedDeltaTime),
                    0f, 100f);
            }
            else
            {
                playerModel.warmth.Value = Mathf.Clamp(
                    (playerModel.warmth.Value - warmUpSpeed * 5 * Time.fixedDeltaTime),
                    0f, 100f);
            }

            playerModel.invincibleTime.Value = Mathf.Clamp((playerModel.invincibleTime.Value - Time.fixedDeltaTime),
                0f, 3f);
        }

        protected override void EarlyGlobalSuperUpdate()
        {
            bool acquiringGround = superCharacterController.currentGround.IsGrounded(false, 0.01f);
            bool maintainingGround = superCharacterController.currentGround.IsGrounded(true, 0.5f);

            if (acquiringGround)
            {
                StartAction("AcquiringGround");
            }
            else
            {
                EndAction("AcquiringGround");
            }

            if (maintainingGround)
            {
                StartAction("MaintainingGround");
            }
            else
            {
                EndAction("MaintainingGround");
            }
        }

        protected override void LateGlobalSuperUpdate()
        {
            // If the movement controller itself is disabled, this shouldn't run.
            if (!enabled || isBeamRotating)
            {
                return;
            }

            // Move the player by our velocity every frame.
            transform.position += playerModel.currentVelocity.Value * superCharacterController.deltaTime;

            // If alive and is moving, set animator.
            if (!isDead && canMove)
            {
                if (playerModel.currentVelocity.Value.magnitude > 0f)
                {
                    animator.SetFloat("Velocity X", 0);
                    animator.SetFloat("Velocity Z",
                        transform.InverseTransformDirection(playerModel.currentVelocity.Value).z *
                        movementAnimationMultiplier);
                    animator.SetBool("Moving", true);
                }
                else
                {
                    animator.SetFloat("Velocity X", 0f);
                    animator.SetFloat("Velocity Z", 0f);
                    animator.SetBool("Moving", false);
                }
            }

            // Facing.
            if (isFacing)
            {
                RotateTowardsDirection(faceInput);
            }
            else if (canMove)
            {
                RotateTowardsMovementDir();
            }

            if (currentState == null && CanStartAction("Idle"))
            {
                StartAction("Idle");
            }

            // Update animator with local movement values.
            animator.SetFloat("Velocity X",
                transform.InverseTransformDirection(playerModel.currentVelocity.Value).x * movementAnimationMultiplier);
            animator.SetFloat("Velocity Z",
                transform.InverseTransformDirection(playerModel.currentVelocity.Value).z * movementAnimationMultiplier);
        }

        private void LateUpdate()
        {
            // Update Animator animation speed.
            animator.SetFloat("AnimationSpeed", animationSpeed);
        }

        #endregion

        #region Inputs

        public void SetMoveInput(Vector3 _moveInput)
        {
            this._moveInput = _moveInput;

            // Forward vector relative to the camera along the x-z plane.
            Vector3 forward = Camera.main.transform.TransformDirection(Vector3.forward);
            forward.y = 0;
            forward = forward.normalized;

            // Right vector relative to the camera always orthogonal to the forward vector.
            Vector3 right = new Vector3(forward.z, 0, -forward.x);
            Vector3 relativeVelocity = _moveInput.x * right + _moveInput.y * forward;

            // Reduce input for diagonal movement.
            if (relativeVelocity.magnitude > 1)
            {
                relativeVelocity.Normalize();
            }

            _cameraRelativeInput = relativeVelocity;
        }

        public void SetJumpInput(Vector3 _jumpInput)
        {
            this._jumpInput = _jumpInput;
        }

        public void SetFaceInput(Vector3 _faceInput)
        {
            this._faceInput = _faceInput;
        }

        #endregion

        #region Actions

        #region Handler

        public void SetHandler(string action, IActionHandler handler)
        {
            actionHandlers[action] = handler;
        }

        public IActionHandler GetHandler(string action)
        {
            if (HandlerExists(action))
            {
                return actionHandlers[action];
            }

            Debug.LogError("PlayerController: No handler for action \"" + action + "\"");
            return actionHandlers["Null"];
        }

        public bool HandlerExists(string action)
        {
            return actionHandlers.ContainsKey(action);
        }

        public bool IsActive(string action)
        {
            return GetHandler(action).IsActive();
        }

        public bool CanStartAction(string action)
        {
            return GetHandler(action).CanStartAction(this);
        }

        public bool CanEndAction(string action)
        {
            return GetHandler(action).CanEndAction(this);
        }

        public void StartAction(string action, object context = null)
        {
            GetHandler(action).StartAction(this, context);
        }

        public void EndAction(string action)
        {
            GetHandler(action).EndAction(this);
        }

        #endregion

        public void Pickup()
        {
            animator.SetInteger("Action", 2);
            SetAnimatorTrigger(AnimatorTrigger.ActionTrigger);
            Lock(true, true, true, 0, 1.4f);
        }

        public void Activate()
        {
            animator.SetInteger("Action", 3);
            SetAnimatorTrigger(AnimatorTrigger.ActionTrigger);
            Lock(true, true, true, 0, 1.2f);
        }

        public void Boost()
        {
            animator.SetInteger("Action", 9);
            SetAnimatorTrigger(AnimatorTrigger.ActionTrigger);
            Lock(true, true, true, 0, 1f);
        }

        public void Dash()
        {
            if (dashForce < 1) return;
            Vector3 planarMoveDirection =
                Math3d.ProjectVectorOnPlane(superCharacterController.up, playerModel.currentVelocity.Value);
            Vector3 verticalMoveDirection = playerModel.currentVelocity.Value - planarMoveDirection;

            playerModel.currentVelocity.Value = cameraRelativeInput * inAirSpeed * dashForce + verticalMoveDirection;
            Lock(false, true, true, 0, 0.5f, () =>
            {
                Vector3 planarMoveDirection =
                    Math3d.ProjectVectorOnPlane(superCharacterController.up, playerModel.currentVelocity.Value);
                Vector3 verticalMoveDirection = playerModel.currentVelocity.Value - planarMoveDirection;
                playerModel.currentVelocity.Value = new Vector3(playerModel.currentVelocity.Value.x / dashForce,
                    verticalMoveDirection.y,
                    playerModel.currentVelocity.Value.z / dashForce);
            });
        }

        public void Death()
        {
            SetAnimatorTrigger(AnimatorTrigger.DeathTrigger);
            Lock(true, true, false, 0.1f, 99f);
        }

        public void Revive()
        {
            SetAnimatorTrigger(AnimatorTrigger.ReviveTrigger);
            Lock(true, true, true, 0f, 1f);
        }

        public void StartFace()
        {
        }

        public void EndFace()
        {
        }

        public void StartInjured()
        {
            animator.SetBool("Injured", true);
        }

        public void EndInjured()
        {
            animator.SetBool("Injured", false);
        }


        public void StartSprint()
        {
            animator.SetBool("Sprint", true);
            _canStrafe = false;
        }

        public void EndSprint()
        {
            animator.SetBool("Sprint", false);
            _canStrafe = true;
        }

        public void StartPushPull()
        {
            animator.SetBool("PushPull", true);
            _canStrafe = false;
        }

        public void EndPushPull()
        {
            animator.SetBool("PushPull", false);
            _canStrafe = true;
        }

        public void StartBeamRotating()
        {
            // animator.SetBool("PushPull", true);
            // _canStrafe = false;
        }

        public void EndBeamRotating()
        {
            // animator.SetBool("PushPull", false);
            // _canStrafe = true;
        }

        public void StartStrafe()
        {
        }

        public void EndStrafe()
        {
        }

        #endregion

        #region Animation

        public void SetAnimatorTrigger(AnimatorTrigger trigger)
        {
            if (showDebug)
            {
                Debug.Log("SetAnimatorTrigger: " + trigger + " - " + (int) trigger);
            }

            animator.SetInteger("TriggerNumber", (int) trigger);
            animator.SetTrigger("Trigger");
        }

        #endregion

        #region Coroutine

        public void LockMovement()
        {
            playerModel.currentVelocity.Value = new Vector3(0, 0, 0);
            animator.SetBool("Moving", false);
            animator.applyRootMotion = true;
        }

        public void UnlockMovement()
        {
            animator.applyRootMotion = false;
        }

        public void Lock(bool lockMovement, bool lockAction, bool timed, float delayTime, float lockTime)
        {
            StopCoroutine("_Lock");
            StartCoroutine(_Lock(lockMovement, lockAction, timed, delayTime, lockTime));
        }

        public void Lock(bool lockMovement, bool lockAction, bool timed, float delayTime, float lockTime,
            [NotNull] Action callback)
        {
            StopCoroutine("_Lock");
            StartCoroutine(_Lock(lockMovement, lockAction, timed, delayTime, lockTime, callback));
        }

        private IEnumerator _Lock(bool lockMovement, bool lockAction, bool timed, float delayTime, float lockTime)
        {
            if (delayTime > 0)
            {
                yield return new WaitForSeconds(delayTime);
            }

            if (lockMovement)
            {
                _canMove = false;
                OnLockMovement();
            }

            if (lockAction)
            {
                _canAction = false;
            }

            if (timed)
            {
                if (lockTime > 0)
                {
                    yield return new WaitForSeconds(lockTime);
                }

                Unlock(lockMovement, lockAction);
            }
        }

        private IEnumerator _Lock(bool lockMovement, bool lockAction, bool timed, float delayTime, float lockTime,
            [NotNull] Action callback)
        {
            if (delayTime > 0)
            {
                yield return new WaitForSeconds(delayTime);
            }

            if (lockMovement)
            {
                _canMove = false;
                OnLockMovement();
            }

            if (lockAction)
            {
                _canAction = false;
            }

            if (timed)
            {
                if (lockTime > 0)
                {
                    yield return new WaitForSeconds(lockTime);
                }

                callback.Invoke();
                Unlock(lockMovement, lockAction);
            }
        }

        public void Unlock(bool movement, bool actions)
        {
            if (movement)
            {
                _canMove = true;
                OnUnlockMovement();
            }

            if (actions)
            {
                _canAction = true;
            }
        }

        #endregion

        #region Movement

        public void KnockbackForce(Vector3 knockDirection, float knockBackAmount, float variableAmount)
        {
            StartCoroutine(_KnockbackForce(knockDirection, knockBackAmount, variableAmount));
        }

        private IEnumerator _KnockbackForce(Vector3 knockDirection, float knockBackAmount, float variableAmount)
        {
            if (rb == null)
            {
                yield break;
            }

            float startTime = Time.time;
            float elapsed = 0f;

            rb.isKinematic = false;

            while (elapsed < .1f)
            {
                rb.AddForce(knockDirection
                            * ((knockBackAmount + Random.Range(-variableAmount, variableAmount))
                               * knockbackMultiplier * 10), ForceMode.Impulse);
                elapsed = Time.time - startTime;
                yield return null;
            }

            rb.isKinematic = true;
        }

        private void RotateTowardsMovementDir()
        {
            Vector3 movementVector =
                new Vector3(playerModel.currentVelocity.Value.x, 0, playerModel.currentVelocity.Value.z);
            if (movementVector.magnitude > 0.01f)
            {
                transform.rotation = Quaternion.Slerp(transform.rotation,
                    Quaternion.LookRotation(movementVector),
                    Time.deltaTime * rotationSpeed);
            }
        }

        private void RotateTowardsTarget(Vector3 targetPosition)
        {
            Debug.Log("RotateTowardsTarget: " + targetPosition);
            Vector3 lookTarget = new Vector3(targetPosition.x - transform.position.x, 0,
                targetPosition.z - transform.position.z);
            if (lookTarget != Vector3.zero)
            {
                Quaternion targetRotation = Quaternion.LookRotation(lookTarget);
                transform.rotation =
                    Quaternion.Lerp(transform.rotation, targetRotation, Time.deltaTime * rotationSpeed);
            }
        }

        private void RotateTowardsDirection(Vector3 direction)
        {
            Debug.Log("RotateTowardsDirection: " + direction);
            Vector3 lookDirection = new Vector3(direction.x, 0, -direction.y);
            Quaternion lookRotation = Quaternion.LookRotation(lookDirection, Vector3.up);
            transform.rotation = Quaternion.Lerp(transform.rotation, lookRotation, Time.deltaTime * rotationSpeed);
        }

        public void Turn(int direction)
        {
            animator.SetInteger("Action", direction);
            SetAnimatorTrigger(AnimatorTrigger.TurnTrigger);
            Lock(true, true, true, 0, 0.55f);
        }

        public void DiveRoll(int rollNumber)
        {
            animator.SetInteger("Action", rollNumber);
            SetAnimatorTrigger(AnimatorTrigger.DiveRollTrigger);
            Lock(true, true, true, 0, 1f);
        }

        public void Roll(int rollNumber)
        {
            animator.SetInteger("Action", rollNumber);
            SetAnimatorTrigger(AnimatorTrigger.RollTrigger);
            Lock(true, true, true, 0, 0.5f);
        }

        public void Knockback(int direction)
        {
            animator.SetInteger("Action", direction);
            SetAnimatorTrigger(AnimatorTrigger.KnockbackTrigger);
            Lock(true, true, true, 0, 1f);
        }

        public void Knockdown(int direction)
        {
            animator.SetInteger("Action", direction);
            SetAnimatorTrigger(AnimatorTrigger.KnockdownTrigger);
            Lock(true, true, true, 0, 5.25f);
        }

        public void Dodge(int direction)
        {
            animator.SetInteger("Action", direction);
            SetAnimatorTrigger(AnimatorTrigger.DodgeTrigger);
            Lock(true, true, true, 0, 0.55f);
        }

        public void ClimbLadder(int action)
        {
            float duration = 0f;

            switch (action)
            {
                case 1:
                case 2:
                case 6:
                    duration = 1.167f;
                    break;
                case 3:
                case 5:
                    duration = 2.667f;
                    break;
                case 4:
                    duration = 1.0f;
                    break;
                default:
                    return;
            }

            // Lock actions when getting on the ladder.
            if (action == 5 || action == 6)
            {
                Lock(false, true, false, 0f, 0f);
            }

            // Trigger animation.
            animator.SetInteger("Action", action);
            SetAnimatorTrigger(AnimatorTrigger.ClimbLadderTrigger);

            // If we are getting off the ladder, we should unlock actions too.
            if (action == 3 || action == 4)
            {
                StartCoroutine(_Lock(true, true, true, 0.1f, duration));
            }
            // Manually start the coroutine to lock movement here so that it doesn't clobber
            // the one we started above to lock actions.
            else
            {
                StartCoroutine(_Lock(true, false, true, 0.1f, duration));
            }
        }

        public void AnimatorMove(Vector3 deltaPosition, Quaternion rootRotation)
        {
            transform.position += deltaPosition;
            transform.rotation = rootRotation;
        }

        #endregion

        #region States

// Below are the state functions. Each one is called based on the name of the state, so when currentState = Idle,
// we call Idle_EnterState. If currentState = Jump, we call Jump_SuperUpdate().

        private void Idle_EnterState()
        {
            superCharacterController.EnableSlopeLimit();
            superCharacterController.EnableClamping();
            canJump = true;
            doublejumped = false;
            canDoubleJump = false;
        }

// Run every frame character is in the idle state.
        private void Idle_SuperUpdate()
        {
            // Check if the character starts falling.
            if (CanStartAction("Fall"))
            {
                StartAction("Fall");
                return;
            }

            // Apply friction to slow to a halt.
            playerModel.currentVelocity.Value = Vector3.MoveTowards(playerModel.currentVelocity.Value, Vector3.zero,
                groundFriction * superCharacterController.deltaTime);

            if (CanStartAction("Move"))
            {
                StartAction("Move");
            }
        }

// Run every frame character is moving.
        private void Move_SuperUpdate()
        {
            // Check if the character starts falling.
            if (CanStartAction("Fall"))
            {
                StartAction("Fall");
                return;
            }

            // Set speed determined by movement type.
            if (canMove)
            {
                float moveSpeed = runSpeed;
                float moveAccel = runAccel;

                if (isSprinting)
                {
                    moveSpeed = sprintSpeed;
                    moveAccel = sprintAccel;
                }
                else if (isPushPulling)
                {
                    moveSpeed = pushPullSpeed;
                    moveAccel = pushPullAccel;
                }

                playerModel.currentVelocity.Value = Vector3.MoveTowards(playerModel.currentVelocity.Value,
                    cameraRelativeInput * moveSpeed,
                    moveAccel * superCharacterController.deltaTime);
            }

            // TODO: XMUG no need, blend animation works fine
            // If there is no movement detected, go into Idle.
            // if (CanStartAction("Idle")) {  StartAction("Idle"); }
        }

        private void Jump_EnterState()
        {
            superCharacterController.DisableClamping();
            superCharacterController.DisableSlopeLimit();

            playerModel.currentVelocity.Value = new Vector3(playerModel.currentVelocity.Value.x, jumpSpeed,
                playerModel.currentVelocity.Value.z);

            animator.SetInteger("Jumping", 1);
            SetAnimatorTrigger(AnimatorTrigger.JumpTrigger);
            canJump = false;
        }

        private void Jump_SuperUpdate()
        {
            if (isBeamRotating) return;

            holdingJump = jumpInput.y != 0f;

            // Cap jump speed if we stop holding the jump button.
            if (!holdingJump && playerModel.currentVelocity.Value.y > (jumpInput.y*jumpSpeed / 4f))
            {
                playerModel.currentVelocity.Value = Vector3.MoveTowards(playerModel.currentVelocity.Value, new Vector3(
                        playerModel.currentVelocity.Value.x,
                        (jumpInput.y*jumpSpeed / 4f), playerModel.currentVelocity.Value.z),
                    fallGravity * superCharacterController.deltaTime);
            }

            Vector3 planarMoveDirection =
                Math3d.ProjectVectorOnPlane(superCharacterController.up, playerModel.currentVelocity.Value);
            Vector3 verticalMoveDirection = playerModel.currentVelocity.Value - planarMoveDirection;

            // Falling.
            // a little bit of offset
            if (playerModel.currentVelocity.Value.y < 1)
            {
                playerModel.currentVelocity.Value = planarMoveDirection;
                currentState = PlayerState.Fall;
                return;
            }

            planarMoveDirection = Vector3.MoveTowards(planarMoveDirection,
                cameraRelativeInput * inAirSpeed,
                inAirAccel * superCharacterController.deltaTime);

            verticalMoveDirection -= superCharacterController.up * jumpGravity * superCharacterController.deltaTime;
            playerModel.currentVelocity.Value = planarMoveDirection + verticalMoveDirection;
        }

        private void DoubleJump_EnterState()
        {
            playerModel.currentVelocity.Value = new Vector3(playerModel.currentVelocity.Value.x, doubleJumpSpeed,
                playerModel.currentVelocity.Value.z);
            canDoubleJump = false;
            doublejumped = true;
            animator.SetInteger("Jumping", 3);
            SetAnimatorTrigger(AnimatorTrigger.JumpTrigger);
        }

        private void DoubleJump_SuperUpdate()
        {
            Jump_SuperUpdate();
        }

        private void Fall_EnterState()
        {
            if (!doublejumped)
            {
                canDoubleJump = true;
            }

            superCharacterController.DisableClamping();
            superCharacterController.DisableSlopeLimit();
            canJump = false;
            animator.SetInteger("Jumping", 2);
            SetAnimatorTrigger(AnimatorTrigger.JumpTrigger);
        }

        private void Fall_SuperUpdate()
        {
            if (isBeamRotating) return;

            if (CanStartAction("Idle"))
            {
                playerModel.currentVelocity.Value =
                    Math3d.ProjectVectorOnPlane(superCharacterController.up, playerModel.currentVelocity.Value);
                StartAction("Idle");
                return;
            }

            // If FallingControl is enabled.
            if (fallingControl)
            {
                Vector3 planarMoveDirection =
                    Math3d.ProjectVectorOnPlane(superCharacterController.up, playerModel.currentVelocity.Value);
                Vector3 verticalMoveDirection = playerModel.currentVelocity.Value - planarMoveDirection;

                planarMoveDirection = Vector3.MoveTowards(planarMoveDirection,
                    cameraRelativeInput * inAirSpeed,
                    inAirAccel * superCharacterController.deltaTime);

                verticalMoveDirection -= superCharacterController.up * fallGravity * superCharacterController.deltaTime;
                playerModel.currentVelocity.Value = planarMoveDirection + verticalMoveDirection;
            }
            else
            {
                playerModel.currentVelocity.Value -=
                    superCharacterController.up * fallGravity * superCharacterController.deltaTime;
            }
        }

        private void Fall_ExitState()
        {
            animator.SetInteger("Jumping", 0);
            SetAnimatorTrigger(AnimatorTrigger.JumpTrigger);
        }

        private void DiveRoll_EnterState()
        {
            OnUnlockMovement += IdleOnceAfterMoveUnlock;
        }

        private void DiveRoll_SuperUpdate()
        {
            if (CanStartAction("Idle"))
            {
                playerModel.currentVelocity.Value =
                    Math3d.ProjectVectorOnPlane(superCharacterController.up, playerModel.currentVelocity.Value);
                StartAction("Idle");
                return;
            }

            playerModel.currentVelocity.Value -=
                superCharacterController.up * (fallGravity / 2) * superCharacterController.deltaTime;
        }

        #endregion

        #region Combat

        public void GetHit(int hitNumber)
        {
            animator.SetInteger("Action", hitNumber);
            SetAnimatorTrigger(AnimatorTrigger.GetHitTrigger);
            Lock(true, true, true, 0.1f, 0.4f);
        }

        public void Attack(int attackNumber, int attackSide, int leftWeapon, int rightWeapon, float duration)
        {
            animator.SetInteger("AttackSide", attackSide);
            Lock(true, true, true, 0, duration);

            Invoke(nameof(AttackGhost), 0.25f);

            // Trigger the animation.
            animator.SetInteger("Action", attackNumber);
            if (attackSide == 3)
            {
                SetAnimatorTrigger(AnimatorTrigger.AttackDualTrigger);
            }
            else
            {
                SetAnimatorTrigger(AnimatorTrigger.AttackTrigger);
            }
        }

        private void AttackGhost()
        {
            GameCore.Get<EventSystem>().Trigger(new PlayerAttackGhostEvent());
        }

        public void RunningAttack(int attackSide, bool leftWeapon, bool rightWeapon, bool dualWeapon,
            bool twoHandedWeapon)
        {
            GameCore.Get<EventSystem>().Trigger(new PlayerAttackGhostEvent());
            if (attackSide == 1 && leftWeapon)
            {
                animator.SetInteger("Action", 1);
                SetAnimatorTrigger(AnimatorTrigger.AttackTrigger);
            }
            else if (attackSide == 2 && rightWeapon)
            {
                animator.SetInteger("Action", 4);
                SetAnimatorTrigger(AnimatorTrigger.AttackTrigger);
            }
            else if (attackSide == 3 && dualWeapon)
            {
                animator.SetInteger("Action", 1);
                SetAnimatorTrigger(AnimatorTrigger.AttackDualTrigger);
            }
            else if (twoHandedWeapon)
            {
                animator.SetInteger("Action", 1);
                SetAnimatorTrigger(AnimatorTrigger.AttackTrigger);
            }
            else
            {
                animator.SetInteger("Action", 1);
                animator.SetInteger("AttackSide", attackSide);
                SetAnimatorTrigger(AnimatorTrigger.AttackTrigger);
            }
        }

        #endregion

        #region TriggerEvent

        private void OnTriggerEnter(Collider collide)
        {
            // // Near a ladder.
            // else if (collide.transform.parent != null) {
            //     if (collide.transform.parent.name.Contains("Ladder")) {
            //         isNearLadder = true;
            //         ladder = collide;
            //     }
            // }
            //
            // // Near a cliff.
            // else if (collide.transform.name.Contains("Cliff")) {
            //     isNearCliff = true;
            //     cliff = collide;
            // }
        }

        private void OnTriggerExit(Collider collide)
        {
            // // Leaving a ladder.
            // else if (collide.transform.parent != null) {
            //     if (collide.transform.parent.name.Contains("Ladder")) {
            //         isNearLadder = false;
            //         ladder = null;
            //     }
            // }
            // // Leaving a cliff.
            // else if (collide.transform.name.Contains("Cliff")) {
            //     isNearCliff = false;
            //     cliff = null;
            // }
        }

        #endregion

        #region Utility

        public void RandomIdle()
        {
            if (idleAlert && isIdle && !isRelaxed && !isAiming)
            {
                animator.SetInteger("Action", 1);
                SetAnimatorTrigger(AnimatorTrigger.IdleTrigger);
            }
        }

        public void IdleOnceAfterMoveUnlock()
        {
            StartAction("Idle");
            OnUnlockMovement -= IdleOnceAfterMoveUnlock;
        }

        public void InstantSwitchOnceAfterMoveUnlock()
        {
            SetAnimatorTrigger(AnimatorTrigger.InstantSwitchTrigger);
            OnUnlockMovement -= InstantSwitchOnceAfterMoveUnlock;
        }

        #endregion
    }
}