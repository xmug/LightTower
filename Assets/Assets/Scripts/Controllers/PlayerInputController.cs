﻿using System;
using Assets.Scripts.Events;
using Assets.Scripts.Models;
using RPGCharacterAnims;
using RPGCharacterAnims.Actions;
using UnityEngine;
using XMUGFramework;

namespace Assets.Scripts.Controllers
{
    [RequireComponent(typeof(PlayerController))]
    public class PlayerInputController : AController
    {
        private PlayerController playerController;
        private PlayerModel playerModel;

        // Inputs.
        private float inputHorizontal = 0;
        private float inputVertical = 0;
        private bool inputJump;
        private bool inputRefire;
        private bool inputActivate;
        private bool inputAttackL;
        private bool inputAttackR;
        private bool inputCastL;
        private bool inputCastR;
        private float inputSwitchUpDown;
        private float inputSwitchLeftRight;
        private bool inputPickup;
        private bool inputAiming;
        private bool inputFace;
        private float inputFacingHorizontal;
        private float inputFacingVertical;
        private bool inputRoll;
        private bool inputShield;
        private bool inputRelax;
        private bool inputSprint;
        private bool inputSwitchWeapon;
        private bool inputDash;


        // Variables.
        private Vector3 moveInput;
        private bool isJumpHeld;
        private Vector3 currentAim;
        private float bowPull;
        private bool blockToggle;
        private float inputPauseTimeout = 0;
        private bool inputPaused = false;
        private bool inputSwitchItem;

        private void Awake()
        {
            playerModel = GameCore.Get<PlayerModel>();
            currentAim = Vector3.zero;
        }

        private void Start()
        {
            playerController = GetComponent<PlayerController>();
        }

        private void Update()
        {
            if (inputPaused)
            {
                if (Time.time > inputPauseTimeout)
                {
                    inputPaused = false;
                }
                else
                {
                    return;
                }
            }

            Inputs();
            // Blocking();
            Moving();
            Emote();

            Strafing();
            Sprinting();
            Dashing();
            Facing();
            // Aiming();
            Rolling();
            Attacking();
        }


        private void Inputs()
        {
            Cursor.lockState = CursorLockMode.Locked;
            try
            {
                inputJump = Input.GetButtonDown("Jump");
                isJumpHeld = Input.GetButton("Jump");
                inputRefire = Input.GetButtonDown("Refire");
                inputActivate = Input.GetButtonDown("Activate");
                //inputShield = Input.GetButtonDown("Roll");
                inputAttackL = Input.GetButtonDown("AttackL");
                inputAttackR = Input.GetButtonDown("AttackR");
                inputPickup = Input.GetButtonDown("Pickup");
                inputAiming = Input.GetButton("Aiming");
                inputSprint = Input.GetButton("Sprint");
                inputHorizontal = Input.GetAxisRaw("Horizontal");
                inputVertical = Input.GetAxisRaw("Vertical");
                inputFace = Input.GetMouseButton(1);
                inputRoll = Input.GetButtonDown("L3");
                // inputSwitchItem = Input.GetKeyDown(KeyCode.E);
                inputDash = Input.GetKeyDown(KeyCode.Tab);

                inputRelax = Input.GetButtonDown("Relax");
                inputFacingHorizontal = Input.GetAxisRaw("FacingHorizontal");
                inputFacingVertical = Input.GetAxisRaw("FacingVertical");
                inputCastL = Input.GetButtonDown("CastL");
                inputCastR = Input.GetButtonDown("CastR");
                inputSwitchUpDown = Input.GetAxisRaw("SwitchUpDown");
                inputSwitchLeftRight = Input.GetAxisRaw("SwitchLeftRight");
                // Injury toggle.
                if (playerController.HandlerExists("Injure"))
                {
                    if (Input.GetKeyDown(KeyCode.I))
                    {
                        if (playerController.CanStartAction("Injure"))
                        {
                            playerController.StartAction("Injure");
                        }
                        else if (playerController.CanEndAction("Injure"))
                        {
                            playerController.EndAction("Injure");
                        }
                    }
                }

                // Slow time toggle.
                if (playerController.HandlerExists("SlowTime"))
                {
                    if (Input.GetKeyDown(KeyCode.T))
                    {
                        if (playerController.CanStartAction("SlowTime"))
                        {
                            playerController.StartAction("SlowTime", 0.0125f);
                        }
                        else if (playerController.CanEndAction("SlowTime"))
                        {
                            playerController.EndAction("SlowTime");
                        }
                    }

                    // Pause toggle.
                    if (Input.GetKeyDown(KeyCode.P))
                    {
                        if (playerController.CanStartAction("SlowTime"))
                        {
                            playerController.StartAction("SlowTime", 0f);
                        }
                        else if (playerController.CanEndAction("SlowTime"))
                        {
                            playerController.EndAction("SlowTime");
                        }
                    }
                }
            }
            catch (System.Exception)
            {
                Debug.LogError("Inputs not found! ");
            }
        }
        
        private void Emote()
        {
            // Check if Emote Combat Action exists.
            if (playerController.HandlerExists("EmoteCombat")) {
                if (playerController.CanStartAction("EmoteCombat")) {
                    string emote = "";
                    if (inputPickup) { emote = "Pickup"; }
                    if (inputActivate)
                    {
                        GameCore.Get<EventSystem>().Trigger(new PlayerActivateEvent());
                    }
                    if (inputRefire) { emote = "Boost"; }

                    if (inputDash)
                    {
                        emote = "Dash";}

                    if (emote != "") { playerController.StartAction("EmoteCombat", emote); }
                }
            }
        }

        public void Attacking()
        {
            if (!playerController.HandlerExists("Attack")) return;

            if (inputAttackR)
            {
                // if (playerController.hasRightWeapon || playerController.rightWeapon == (int) Weapon.Unarmed)
                // {
                //     playerController.StartAction("Attack", new AttackContext("Attack", "Left"));
                // }
                if(playerController.CanStartAction("Attack"))
                {
                    playerController.StartAction("Attack", new AttackContext("Attack", "Left"));
                }
            }
        }

        public void Moving()
        {
            moveInput = new Vector3(inputHorizontal, inputVertical, 0f);
            playerController.SetMoveInput(moveInput);

            // Set the input on the jump axis every frame.
            Vector3 jumpInput = isJumpHeld ? Vector3.up : Vector3.zero;
            playerController.SetJumpInput(jumpInput);

            // If we pressed jump button this frame, jump.
            if (playerController.HandlerExists("Jump"))
            {
                if (inputJump && playerController.CanStartAction("Jump"))
                {
                    playerController.StartAction("Jump");
                }
                else if (inputJump && playerController.CanStartAction("DoubleJump"))
                {
                    playerController.StartAction("DoubleJump");
                }
            }
        }

        private void Sprinting()
        {
            // Check to make sure Strafe Action exists.
            if (!playerController.HandlerExists("Sprint"))
            {
                return;
            }

            if (inputSprint)
            {
                if (playerController.CanStartAction("Sprint"))
                {
                    playerController.StartAction("Sprint");
                }
            }
            else
            {
                if (playerController.CanEndAction("Sprint"))
                {
                    playerController.EndAction("Sprint");
                }
            }
        }
        
        private void Dashing()
        {
            // Check to make sure Strafe Action exists.
            if (!playerController.HandlerExists("Dash"))
            {
                return;
            }

            if (inputDash)
            {
                if (playerController.CanStartAction("Dash"))
                {
                    playerController.StartAction("Dash");
                }
            }
            else
            {
                if (playerController.CanEndAction("Dash"))
                {
                    playerController.EndAction("Dash");
                }
            }
        }

        private void Strafing()
        {
            // Check to make sure Strafe Action exists.
            if (!playerController.HandlerExists("Strafe"))
            {
                return;
            }

            if (playerController.canStrafe)
            {
                if (inputAiming)
                {
                    if (playerController.CanStartAction("Strafe"))
                    {
                        playerController.StartAction("Strafe");
                    }
                }
                else
                {
                    if (playerController.CanEndAction("Strafe"))
                    {
                        playerController.EndAction("Strafe");
                    }
                }
            }
        }

        private void Facing()
        {
            // Check to make sure Face Action exists.
            if (!playerController.HandlerExists("Face"))
            {
                return;
            }

            if (playerController.canFace)
            {
                if (HasFacingInput())
                {
                    if (inputFace)
                    {
                        // Get world position from mouse position on screen and convert to direction from character.
                        Plane playerPlane = new Plane(Vector3.up, transform.position);
                        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                        float hitdist = 0.0f;
                        if (playerPlane.Raycast(ray, out hitdist))
                        {
                            Vector3 targetPoint = ray.GetPoint(hitdist);
                            Vector3 lookTarget = new Vector3(targetPoint.x - transform.position.x,
                                transform.position.z - targetPoint.z, 0);
                            playerController.SetFaceInput(lookTarget);
                        }
                    }
                    else
                    {
                        playerController.SetFaceInput(new Vector3(inputFacingHorizontal, inputFacingVertical, 0));
                    }

                    if (playerController.CanStartAction("Face"))
                    {
                        playerController.StartAction("Face");
                    }
                }
                else
                {
                    if (playerController.CanEndAction("Face"))
                    {
                        playerController.EndAction("Face");
                    }
                }
            }
        }

        public void Rolling()
        {
            if (!inputRoll)
            {
                return;
            }
            
            if (playerController.HandlerExists("Roll"))
            {
                if (!playerController.CanStartAction("Roll"))
                {
                    return;
                }

                playerController.StartAction("Roll", 1);
            }
        }

        public bool HasFacingInput()
        {
            if ((inputFacingHorizontal < -0.05
                 || inputFacingHorizontal > 0.05)
                || (inputFacingVertical < -0.05
                    || inputFacingVertical > 0.05)
                || inputFace)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}