using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Events;
using Assets.Scripts.Models;
using UnityEngine;
using UnityEngine.Events;
using Cinemachine;
using XMUGFramework;
using Random = UnityEngine.Random;

[System.Serializable]
public class CameraEvent : UnityEvent<bool>
{
}

[System.Serializable]
public class EffectEvent : UnityEvent<bool, bool>
{
}

[System.Serializable]
public class ParticleEvent : UnityEvent<bool, int>
{
}

public class WormBossController : ATrap
{
    [HideInInspector] public CameraEvent OnBossReveal;
    [HideInInspector] public EffectEvent GroundContact;
    [HideInInspector] public ParticleEvent GroundDetection;

    [Header("Pathing")] [SerializeField] CinemachineSmoothPath path = default;
    [SerializeField] CinemachineDollyCart cart = default;
    [SerializeField] LayerMask terrainLayer = default;

    [HideInInspector] public Vector3 startPosition, endPosition;

    private PlayerModel model;

    RaycastHit hitInfo;
    int totalHealth = 3;
    public int currentHealth = 0;
    private bool isDead = false;

    // Start is called before the first frame update
    protected override void Awake()
    {
        base.Awake();
        model = GameCore.Get<PlayerModel>();

        GameCore.Get<EventSystem>().Register<BossEvents.BossFightStartEvent>((param => { this.enabled = true; }));

        GameCore.Get<EventSystem>().Register<BossEvents.BossDeathEvent>((param =>
        {
            isDead = true;
            this.enabled = false;
        }));
    }

    void AI()
    {
        UpdatePath(false);
        StartCoroutine(FollowPath());

        IEnumerator FollowPath()
        {
            while (!isDead)
            {
                //play leaving ground effect
                yield return new WaitUntil(() => cart.m_Position >= 0.01f);
                GroundContact.Invoke(true, true);
                yield return new WaitUntil(() => cart.m_Position >= 0.23f);
                GroundContact.Invoke(false, true);

                // wait to reenter ground

                yield return new WaitUntil(() => cart.m_Position >= 0.60f);
                GroundContact.Invoke(true, false);
                yield return new WaitUntil(() => cart.m_Position >= 0.90f);
                GroundContact.Invoke(false, false);
                OnBossReveal.Invoke(false);

                // wait a bit to come out of ground again
                yield return new WaitUntil(() => cart.m_Position >= 0.99f);
                yield return new WaitForSeconds(Random.Range(1, 2));

                //reset path
                UpdatePath(false);
                yield return new WaitUntil(() => cart.m_Position <= 0.05f);

                if (currentHealth <= 0f)
                {
                    GameCore.Get<EventSystem>().Trigger(new BossEvents.BossDeathEvent());
                    yield break;
                }
            }
        }
    }

    private void OnEnable()
    {
        AI();
    }

    private void OnDisable()
    {
        cart.m_Speed = 0f;
        cart.m_Position = 0f;
    }

    void UpdatePath(bool onGoing = true)
    {
        Vector3 relativePlayerPos = model.playerController.transform.position;
        var localPPos = transform.parent.InverseTransformPoint(relativePlayerPos);
        var secPoint = new Vector3(0, localPPos.y * 5 / 6, 0);
        var ManhattanTransfer = new Vector3(0, localPPos.y, 0);

        Ray ray = new Ray(transform.parent.TransformPoint(ManhattanTransfer),
            model.playerController.transform.position - transform.parent.TransformPoint(ManhattanTransfer));
        if (Physics.Raycast(ray, out hitInfo,
            1000, terrainLayer.value))
        {
            endPosition = transform.parent.InverseTransformPoint(hitInfo.point + ray.direction * 30);
            GroundDetection.Invoke(false, 1);
        }

        path.m_Waypoints[0].position = transform.parent.InverseTransformPoint(transform.parent.position);
        path.m_Waypoints[1].position = secPoint;
        path.m_Waypoints[2].position = endPosition;

        path.InvalidateDistanceCache();
        if (!onGoing)
        {
            cart.m_Position = 0;
        }

        //speed
        cart.m_Speed = cart.m_Path.PathLength / 1000;

        currentHealth = totalHealth;

        OnBossReveal.Invoke(true);
    }

    protected override void _impact()
    {
    }

    protected override void _deImpact()
    {
    }

    protected override void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && working && GameCore.Get<PlayerModel>().invincibleTime.Value == 0)
        {
            GameCore.Get<EventSystem>().Trigger(new PlayerGetHitEvent()
            {
                warmthReduce = 100f
            });
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawSphere(startPosition, 1);
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(endPosition, 1);
    }
}