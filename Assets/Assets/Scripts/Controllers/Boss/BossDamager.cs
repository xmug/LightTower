using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossDamager : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<WormBossController>(out var worm))
        {
            worm.currentHealth--;
        }
    }
}
