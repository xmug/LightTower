﻿using System;
using System.Collections;
using Assets.Scripts.Events;
using Assets.Scripts.Models;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using XMUGFramework;

namespace Assets.Scripts.Controllers
{
    public class CameraController : AController
    {
        private Volume postProcessVolume;
        private CoroutineQueue cQueue;
        private ColorAdjustments caj;

        private void Awake()
        {
            cQueue = new CoroutineQueue(1, StartCoroutine);
            postProcessVolume = GetComponent<Volume>();
            caj = postProcessVolume.profile.components.Find((component =>
            {
                switch (component)
                {
                    case ColorAdjustments caj:
                        return true;
                    default:
                        return false;
                }
            })) as ColorAdjustments;
            
            if (caj)
            {
                GameCore.Get<EventSystem>().Register<PlayerDeathEvent>(param =>
                {
                    var time = GameCore.Get<PlayerModel>().reviveTime;
                    cQueue.Run(FadeIn(time*2/3));
                    cQueue.Run(FadeOut(time/3));
                });
            }
        }

        IEnumerator FadeIn(float time)
        {
            float ticker = 0;
            while (ticker < time)
            {
                caj.postExposure.value -= 11f * Time.fixedDeltaTime / time;
                yield return null;
                ticker += Time.fixedDeltaTime;
            }
        }
        
        IEnumerator FadeOut(float time)
        {
            GameCore.Get<EventSystem>().Trigger(new PlayerReviveEvent());
            float ticker = 0;
            while (ticker < time)
            {
                caj.postExposure.value += 11f * Time.fixedDeltaTime / time;
                yield return null;
                ticker += Time.fixedDeltaTime;
            }
        }
    }
}