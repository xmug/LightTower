﻿using System;
using Assets.Scripts.Events;
using UnityEngine;
using XMUGFramework;

namespace Assets.Scripts.Controllers.Mechanism
{
    public class HUDInfo : AController
    {
        public string Info;

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                GameCore.Get<EventSystem>().Trigger(new PlayerHUDUpdateEvent()
                {
                    info = Info
                });
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                GameCore.Get<EventSystem>().Trigger(new PlayerHUDUpdateEvent()
                {
                    info = ""
                });
            }
        }
    }
}