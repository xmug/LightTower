using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Events;
using UnityEngine;
using XMUGFramework;

public class MirrorController : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("SXXBoss"))
        {
            GameCore.Get<EventSystem>().Trigger(new BossEvents.SXXBossMirrorContatctEvent());
        }
    }
}
