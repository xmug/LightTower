using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Events;
using UnityEngine;
using XMUGFramework;

public class MovePlatformController : ATrap
{
    private List<GameObject> targetsList = new List<GameObject>();
    public bool isStepUpMove = false;
    public GameObject mesh;
    public GameObject[] targets;
    [SerializeField] private float speed;
    public float waitingTime =3f;
    private Vector3 curPos;
    public bool isWaiting;
    public bool isReWinding = false;

    protected override void Awake()
    {
        base.Awake();
        if (!mesh) mesh = gameObject;
        foreach (var target in targets)
        {
            targetsList.Add(target);
        }

        targetsList.Add(transform.parent.gameObject);

        if (isStepUpMove)
        {
            isWaiting = true;
            foreach (var aTrap in chainTraps)
            {
                var p = aTrap as MovePlatformController;
                if (p)
                {
                    p.isWaiting = true;
                }
            }
        }

        GameCore.Get<EventSystem>().Register<PlayerDeathEvent>((param => { isStepUpMove = false; }));
    }

    protected override IEnumerator Interval()
    {
        while (gameObject.activeInHierarchy)
        {
            if (isImpacting)
            {
                _impact();
                ChainImpact(true);
                yield return new UnityEngine.WaitUntil(() =>
                {
                    bool rt = true;
                    rt &= !isImpacting;
                    foreach (var trap in chainTraps)
                    {
                        rt = rt && !trap.isImpacting;
                    }
                    return rt;
                });
            }
            else
            {
                _deImpact();
                ChainImpact(false);
                if (isReWinding)
                {
                    yield return wfsST;
                }
                else
                {
                    yield return wfsST;
                }
            }
        }
    }

    protected override void _impact()
    {
        StartCoroutine(move());

        IEnumerator move()
        {
            while (true)
            {
                var tmpSpeed = Mathf.Clamp(speed * Time.fixedDeltaTime, 0f,
                    Vector3.Distance(curPos, mesh.transform.position));
                mesh.transform.position += (curPos - mesh.transform.position).normalized * tmpSpeed;
                if (tmpSpeed < 0.001f)
                {
                    isImpacting = false;
                    yield break;
                }   

                if (isWaiting)
                {
                    yield return new UnityEngine.WaitUntil(() => { return !isWaiting; });
                }
                yield return new WaitForFixedUpdate();
            }
        }
    }

    protected override void _deImpact()
    {
        if (!isReWinding)
        {
            Switch();
        }
        else
        {
            RewindSwitch();
        }

        isImpacting = true;
    }

    private void Switch()
    {
        curPos = LoopGet();
    }

    private void RewindSwitch()
    {
        curPos = ReWindLoopGet();
    }

    private Vector3 LoopGet()
    {
        var tmp = targetsList.First();
        if (tmp == transform.parent.gameObject)
        {
            isReWinding = true;
            var t = tmp.transform.position;
            return t;
        }

        targetsList.Remove(tmp);
        var rt = tmp.transform.position;
        targetsList.Add(tmp);


        return rt;
    }

    private Vector3 ReWindLoopGet()
    {
        var tmp = targetsList.Last();
        if (tmp == transform.parent.gameObject)
        {
            isReWinding = false;
            var t = tmp.transform.position;
            return t;
        }

        targetsList.Remove(tmp);
        var rt = tmp.transform.position;
        targetsList.Insert(0, tmp);


        return rt;
    }


    protected override void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (isStepUpMove)
            {
                isWaiting = false;
                isReWinding = false;
                foreach (var aTrap in chainTraps)
                {
                    var p = aTrap as MovePlatformController;
                    if (p)
                    {
                        p.isWaiting = false;
                        p.isReWinding = false;
                    }
                }
            }
        }
    }

    protected void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (isStepUpMove)
            {
                isWaiting = true;
                foreach (var aTrap in chainTraps)
                {
                    var p = aTrap as MovePlatformController;
                    if (p)
                    {
                        p.isWaiting = true;
                    }
                }
                StartCoroutine(timer());
            }

            IEnumerator timer()
            {
                float counter = 0f;
                while (counter < waitingTime)
                {
                    if (!isWaiting)
                    {
                        yield break;
                    }

                    counter += Time.fixedDeltaTime;
                    yield return null;
                }

                isReWinding = true;
                isWaiting = false;
                
                foreach (var aTrap in chainTraps)
                {
                    var p = aTrap as MovePlatformController;
                    if (p)
                    {
                        p.isReWinding = true;
                        p.isWaiting = false;
                    }
                }
            }
        }
    }
}