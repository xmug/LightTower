using Assets.Scripts.Events;
using Assets.Scripts.Models;
using UnityEngine;
using XMUGFramework;

public class BonfireController : AInteractable
{
    public GameObject subObject;
    [SerializeField] private bool isActive;
    public bool isSavePoint = false;
    private PlayerModel model;
    private Vector3 bonfireLitPos;

    protected override void Awake()
    {
        base.Awake();
        model = GameCore.Get<PlayerModel>();

        if (isActive)
        {
            subObject.SetActive(true);
            isActive = true;
        }
    }

    protected override void _activate()
    {
        if(isSavePoint)
        {
            GameCore.Get<GameModel>().savePointPos.Value = bonfireLitPos;
        }
        subObject.SetActive(true);
        isActive = true;
    }

    protected override void _deActivate()
    {
        subObject.SetActive(false);
        isActive = false;
    }

    protected override void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            bonfireLitPos = other.transform.position;
            IsActivatable = true;
            playerCollider = other;
            GameCore.Get<EventSystem>().Trigger(new PlayerHUDUpdateEvent()
            {
                info = HUDInfo
            });
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (isActive && other.CompareTag("Player"))
        {
           model.warmth.Value = Mathf.Lerp(model.warmth.Value, 100f, 20f * Time.fixedDeltaTime);
           model.torchFuel.Value = Mathf.Lerp(model.torchFuel.Value, 100f, 20f * Time.fixedDeltaTime);
        }
    }
}
