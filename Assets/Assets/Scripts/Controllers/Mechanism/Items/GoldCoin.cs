﻿using System;
using Assets.Scripts.Models;
using XMUGFramework;

namespace Assets.Scripts.Controllers
{
    public class GoldCoin : ACollection
    {
        protected override BindableProperty<int> property { get; set; }

        private void Awake()
        {
            property = GameCore.Get<PlayerModel>().goldCoinNum;
        }
    }
}