﻿using System;
using UnityEngine;
using XMUGFramework;

namespace Assets.Scripts.Controllers
{
    public abstract class ACollection : AController
    {
        protected abstract BindableProperty<int> property { get; set; }
        
        // TODO: particle Effect
        public ParticleSystem collectionEffect;
        
        protected void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                property.Value++;
                Destroy(this.gameObject, 0.25f);
            }
        }
    }
}