using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Events;
using Unity.Mathematics;
using UnityEngine;
using VLB;
using VLB_Samples;
using XMUGFramework;

public class ObjectPushPull : AInteractable
{
    public bool isPhysicsAffected = false;
    
    public override bool IsActivatable
    {
        get { return isActivatable || isActivated.Value; }
        set => isActivatable = value;
    }

    protected override void _activate()
    {
        if(isPhysicsAffected)
        {
            if (TryGetComponent<Rigidbody>(out var rb))
            {
                rb.constraints = RigidbodyConstraints.FreezeAll;
            }
        }
    }

    protected override void _deActivate()
    {
        if(isPhysicsAffected)
        {
            if (TryGetComponent<Rigidbody>(out var rb))
            {
                rb.constraints = RigidbodyConstraints.None;
            }
        }
    }
}