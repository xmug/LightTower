using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Controllers.Mechanism;
using Assets.Scripts.Events;
using ParadoxNotion.Design;
using UnityEngine;
using UnityEngine.Playables;
using VLB;
using VLB_Samples;
using XMUGFramework;

public class LightBeamer : AInteractable
{
    public float rotateSpeed = 100f;
    private VolumetricLightBeam subObject;
    public bool isShootingBeam = false;
    private bool isPlayerOnIt = false;
    public GameObject handle;

    [UnityEngine.Header("LightBeamer Activated CG")]
    private PlayableAsset asset;

    protected override void Awake()
    {
        base.Awake();
        subObject = GetComponentInChildren<VolumetricLightBeam>(true);
    }

    public void OnActivate()
    {
        subObject.gameObject.SetActive(true);
        isShootingBeam = true;
        if (asset)
        {
            GameCore.Get<EventSystem>().Trigger(new PlayClipEvent(asset));
        }
    }

    private void FixedUpdate()
    {
        if (isPlayerOnIt)
        {
            Rotate();
        }
    }

    private void Rotate()
    {
        // take over Input
        var verticalInput = Input.GetAxisRaw("Vertical");
        transform.RotateAround(transform.position, transform.up, verticalInput * rotateSpeed * Time.fixedDeltaTime);
    }

    protected override void OnTriggerEnter(Collider other)
    {
        if (isShootingBeam && other.CompareTag("Player"))
        {
            if (Vector3.Distance(other.transform.position, transform.position) >
                2 * Vector3.Distance(transform.position, handle.transform.position))
            {
                return;
            }

            IsActivatable = true;
            playerCollider = other;
        }

        if (other.TryGetComponent<LightBeamer>(out var lightBeamer))
        {
            lightBeamer.OnActivate();
            subObject.GetComponent<MeshCollider>().enabled = false;
        }

        if (other.TryGetComponent<BossFightTrigger>(out var trigger))
        {
            GameCore.Get<EventSystem>().Trigger(new BossEvents.BossFightStartEvent());
        }
    }

    protected override void _activate()
    {
        if (isShootingBeam)
        {
            isPlayerOnIt = true;
            GameCore.Get<EventSystem>().Trigger(new PlayerHUDUpdateEvent()
            {
                info = "W/S to Rotate"
            });
        }
    }

    protected override void _deActivate()
    {
        if (isShootingBeam)
        {
            isPlayerOnIt = false;
            GameCore.Get<EventSystem>().Trigger(new PlayerHUDUpdateEvent()
            {
                info = ""
            });
        }
    }

    private void OnDrawGizmos()
    {
        DebugDraw.DrawVector(transform.position, transform.up, 5f, 5f, Color.yellow, 5f);
    }
}