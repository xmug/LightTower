using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Controllers;
using Assets.Scripts.Models;
using UnityEngine;
using XMUGFramework;

public class BumperPadController : MonoBehaviour
{
    public float padForce = 10f;
    private PlayerModel playerModel;

    private void Awake()
    {
        playerModel = GameCore.Get<PlayerModel>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<PlayerController>(out var controller))
        {
            StartCoroutine(jump());

            IEnumerator jump()
            {
                for (float i = 0; i < 1f; i += Time.fixedDeltaTime)
                {
                    // If we pressed jump button this frame, jump.
                    if (controller.HandlerExists("Jump"))
                    {
                        if (controller.CanStartAction("Jump"))
                        {
                            controller.StartAction("Jump");
                        }
                    }
                    
                    playerModel.currentVelocity.Value = Vector3.MoveTowards(playerModel.currentVelocity.Value, new Vector3(
                            playerModel.currentVelocity.Value.x,
                            padForce, playerModel.currentVelocity.Value.z),
                        controller.fallGravity * Time.fixedDeltaTime);

                    yield return null;
                }
            }
        }
    }
}