using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Controllers;
using Assets.Scripts.Events;
using Assets.Scripts.Models;
using RPGCharacterAnims.Actions;
using UnityEngine;
using UnityEngine.AI;
using XMUGFramework;

public enum GhostState
{
    Idle,
    Detecting,
    Hovering,
    Attacking,
    Die
}

public class GhostController : MonoBehaviour
{
    public float warmthReduce = 34f;
    
    public float speed = 5.0f;
    public float rush = 15.0f;
    public float attackTimer = 3f;

    private float curSpeed;
    private Animator anim;
    private Vector3 oriPos;
    [SerializeField] private Vector3 tarPos;
    [SerializeField] private bool isDetectable;
    [Header("GhostState")] public GhostState ghostState;


    private NavMeshAgent navAgent;
    private bool isCD = false;
    private bool isInRange = false;
    private float ticker = 0;

    private SphereCollider pointCollider;
    private Collider spotCollider;
    private Collider playerCollider;


    private void Awake()
    {
        anim = GetComponentInChildren<Animator>();
        navAgent = GetComponentInChildren<NavMeshAgent>();

        curSpeed = 0f;
        navAgent.destination = transform.position;
        ghostState = GhostState.Idle;
        if (isDetectable)
        {
            oriPos = this.transform.position;
        }
        
        GameCore.Get<EventSystem>().Register<PlayerAttackGhostEvent>(param =>
        {
            if (isInRange)
            {
                ghostState = GhostState.Die;
                Destroy(this.gameObject, 1.5f);
            }
        });
        
        GameCore.Get<EventSystem>().Register<PlayerDeathEvent>(param =>
        {
            curSpeed = 0f;
            navAgent.destination = transform.position;
            ghostState = GhostState.Idle;
        });
    }

    private void Update()
    {
        if (ghostState == GhostState.Idle) return;

        if (ghostState == GhostState.Die)
        {
            Destroy(this.gameObject);
            return;
        }

        if (ghostState == GhostState.Hovering || ghostState == GhostState.Attacking)
        {
            anim.SetBool("Fly Forward", true);
            Move();
        }
        else
        {
            anim.SetBool("Fly Forward", false);
        }
        
        if (isCD)
        {
            ticker += Time.deltaTime;
            isCD = !(ticker > attackTimer);
        }
    }

    private void Move()
    {
        if (ghostState == GhostState.Attacking)
        {
            if (isCD)
            {
                tarPos = Vector3.Lerp(transform.position, transform.position + (transform.position - GameCore.Get<PlayerModel>().playerController.transform.position)
                    .normalized * pointCollider.radius, speed*Time.deltaTime);
                curSpeed = speed;
            }
            else
            {
                tarPos = transform.position + (GameCore.Get<PlayerModel>().playerController.transform.position - transform.position) *
                         1.2f; // offset
                curSpeed = rush;
            }
        }
        
        navAgent.destination = tarPos;
        navAgent.speed = curSpeed;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("PlayerPointCollider"))
        {
            if (ghostState != GhostState.Attacking)
            {
                pointCollider = other as SphereCollider;
                curSpeed = speed;
                tarPos = GameCore.Get<PlayerModel>().playerController.transform.position;
                ghostState = GhostState.Hovering;
            }

            isInRange = true;
        }

        if (other.CompareTag("PlayerSpotCollider"))
        {
            spotCollider = other;
            ghostState = GhostState.Attacking;
        }

        if (other.CompareTag("Player"))
        {
            if(!isCD)
            {
                playerCollider = other;
                isCD = true;
                ticker = 0;
                GameCore.Get<EventSystem>().Trigger(new PlayerGetHitEvent()
                {
                    warmthReduce = this.warmthReduce
                });
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("PlayerPointCollider"))
        {
            isInRange = false;
        }
    }

    private void OnDrawGizmos()
    {
        DebugDraw.DrawMarker(tarPos, 5f, Color.red, 2f);
    }
}