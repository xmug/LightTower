using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinnerController : MonoBehaviour
{
    public float spinSpeed;

    private void FixedUpdate()
    {
        transform.Rotate(transform.up, spinSpeed * Time.fixedDeltaTime);
    }
}
