using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Events;
using UnityEngine;
using UnityEngine.UI;
using XMUGFramework;

public class HUDController : MonoBehaviour
{
    private Text textComp;
    
    private void Awake()
    {
        textComp = GetComponent<Text>();
        textComp.text = "";
        textComp.transform.localScale = new Vector3(-0.25f, 0.25f, 0.25f);
        GameCore.Get<EventSystem>().Register<PlayerHUDUpdateEvent>(param =>
        {
            textComp.text = param.info;
        });
    }

    private void FixedUpdate()
    {
        transform.parent.LookAt(Camera.main.transform);
    }
}
