using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SocialPlatforms;
using VLB;
using VLB_Samples;
using Object = System.Object;

public class TorchLightGenerator : MonoBehaviour
{
    public bool isStatic;
    public bool isPlayer = false;
    public bool useSpot = false;
    public float torchFlickTime = 1f;
    public Color pointLightColor = Color.white;
    [Range(0.01f, 20f)] public float pointLightRange = 10f;
    [Range(0, 1)] public float simulateArea;
    public LayerMask walkableLayer = 9;

    [Header("SphereTorch")] 
    public GameObject customizedPrefab;

    private Light light;

    private void Awake()
    {
        if(!isStatic && useSpot)
        {
            var olds = GetComponentsInChildren<LightSpotTrigger>();
            foreach (var t in olds)
            {
                Destroy(t.gameObject);
            }

            GenerateSpotlightTorch();
        }
    }

    public void GenerateSpotlightTorch()
    {
        var lights = GetComponentsInChildren<Light>();
        for (int i = 0; i < lights.Length; i++)
        {
            if (lights[i].type == LightType.Point)
            {
                light = lights[i];
            }
        }

        if (light == null)
        {
            GameObject pointLightObj = new GameObject();
            pointLightObj.transform.parent = this.transform;
            pointLightObj.transform.localPosition = Vector3.zero;
            pointLightObj.transform.rotation = Quaternion.Euler(90, 0, 0);
            pointLightObj.name = "PointLight";
            if(isPlayer) pointLightObj.tag = "PlayerPointCollider";
            var pointLight = pointLightObj.AddComponent<Light>();
            pointLight.type = LightType.Point;
            pointLight.range = pointLightRange;
            pointLight.lightmapBakeType = LightmapBakeType.Baked;
            pointLight.renderMode = LightRenderMode.ForcePixel;
            pointLight.color = pointLightColor;
            pointLight.transform.localScale = Vector3.one;
            pointLight.intensity = 3;

            var sCollider = pointLightObj.AddComponent<SphereCollider>();
            sCollider.radius = pointLight.range * 2;
            sCollider.isTrigger = true;
            var sphereTrigger = pointLightObj.AddComponent<LightSphereTrigger>();

            light = pointLight;
        }

        GameObject lightBeamObj = new GameObject();
        lightBeamObj.name = "LightTrigger";
        lightBeamObj.transform.parent = this.transform;
        lightBeamObj.transform.rotation = Quaternion.Euler(90, 0, 0);
        lightBeamObj.transform.position =
            transform.parent.position + simulateArea * Vector3.up * light.range * 2;
        lightBeamObj.transform.localScale = Vector3.one;
        if(isPlayer)lightBeamObj.tag = "PlayerSpotCollider";
        var lightBeam = lightBeamObj.AddComponent<VolumetricLightBeam>();
        var spotLight = lightBeamObj.AddComponent<Light>();
        var lightTrigger = lightBeamObj.AddComponent<LightSpotTrigger>();
        var triggerVolume = lightBeamObj.AddComponent<TriggerZone>();
        lightTrigger.originPosY = light.transform.position.y;
        lightTrigger.offsetY = simulateArea * light.range * 2;
        lightTrigger.pointLight = light.gameObject;
        lightTrigger.layerMask = walkableLayer;
        lightTrigger.flickTime = torchFlickTime;

        spotLight.type = LightType.Spot;
        float a = light.range;
        float c = 2 * a;
        float b = Mathf.Sqrt(a * a + c * c);
        float angle = Mathf.Rad2Deg * Mathf.Acos((Mathf.Pow(b, 2) + Mathf.Pow(c, 2) - Mathf.Pow(a, 2)) / (2 * b * c));
        spotLight.lightmapBakeType = LightmapBakeType.Baked;
        spotLight.range = light.range * 2 * 2; // a little offset
        spotLight.shadows = LightShadows.Soft;
        spotLight.bounceIntensity = 0;
        spotLight.lightmapBakeType = LightmapBakeType.Realtime;
        spotLight.renderMode = LightRenderMode.ForcePixel;
        spotLight.spotAngle = 2 * angle;
        spotLight.innerSpotAngle = 2 * angle;
        spotLight.spotAngle = spotLight.innerSpotAngle;
        spotLight.intensity = light.intensity * 10 + 5f;
        spotLight.color = pointLightColor;
        lightBeam.spotAngleFromLight = false;
        lightBeam.spotAngle = spotLight.innerSpotAngle;
        lightBeam.colorFromLight = true;
        lightBeam.intensityInside = 0;
        lightBeam.intensityOutside = 0;
        lightBeam.intensityFromLight = false;
        lightBeam.fallOffEndFromLight = true;
    }

    public void GenerateSphereTorch()
    {
        var lights = GetComponentsInChildren<Light>();
        for (int i = 0; i < lights.Length; i++)
        {
            if (lights[i].type == LightType.Point)
            {
                light = lights[i];
            }
        }

        if (light == null)
        {
            GameObject pointLightObj = new GameObject();
            pointLightObj.transform.parent = this.transform;
            pointLightObj.transform.localPosition = Vector3.zero;
            pointLightObj.transform.rotation = Quaternion.Euler(90, 0, 0);
            pointLightObj.name = "PlayerPointCollider";
            var pointLight = pointLightObj.AddComponent<Light>();
            pointLight.type = LightType.Point;
            pointLight.range = pointLightRange;
            pointLight.lightmapBakeType = LightmapBakeType.Baked;
            pointLight.renderMode = LightRenderMode.ForcePixel;
            pointLight.color = pointLightColor;
            pointLight.transform.localScale = Vector3.one;
            light = pointLight;
        }


        GameObject sphere = new GameObject();
        sphere.name = "PlayerPointCollider";
        sphere.tag = "PlayerPointCollider";
        sphere.transform.parent = this.transform;
        var sphereCollider = sphere.AddComponent<SphereCollider>();
        sphere.AddComponent<LightSphereTrigger>();
        sphereCollider.center = light.transform.position;
        sphereCollider.radius = light.range * 1f;
        sphereCollider.isTrigger = true;
        
        GameObject spot = Instantiate(customizedPrefab, transform);
        spot.name = "PlayerSpotCollider";
        spot.tag = "PlayerSpotCollider";
        spot.transform.localScale = Vector3.one * light.range * simulateArea * 2f;
        var spotCollider = spot.GetComponent<SphereCollider>();
        spot.AddComponent<LightSphereTrigger>();
        spotCollider.isTrigger = true;
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(TorchLightGenerator))]
public class TorchLightGeneratorEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (Application.isPlaying) return;
        if (GUILayout.Button("Generate Spotlight Torch"))
        {
            var taget = (target as TorchLightGenerator);

            var olds = taget.GetComponentsInChildren<LightSpotTrigger>();
            foreach (var t in olds)
            {
                DestroyImmediate(t.gameObject);
            }

            taget.GenerateSpotlightTorch();
        }

        if (GUILayout.Button("Generate Spotlight Torch with New"))
        {
            var taget = (target as TorchLightGenerator);

            var olds = taget.GetComponentsInChildren<LightSpotTrigger>();
            foreach (var t in olds)
            {
                DestroyImmediate(t.gameObject);
            }

            var oolds = taget.GetComponentsInChildren<Light>();
            foreach (var t in oolds)
            {
                if (t.type == LightType.Point)
                    DestroyImmediate(t.gameObject);
            }

            taget.GenerateSpotlightTorch();
        }

        if (GUILayout.Button("Generate Sphere Torch"))
        {
            var taget = (target as TorchLightGenerator);

            var olds = taget.GetComponentsInChildren<LightSphereTrigger>();
            foreach (var t in olds)
            {
                DestroyImmediate(t.gameObject);
            }

            taget.GenerateSphereTorch();
        }
    }
}
#endif