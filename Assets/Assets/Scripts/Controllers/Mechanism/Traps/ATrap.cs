﻿using System.Collections;
using Assets.Scripts.Events;
using Assets.Scripts.Models;
using UnityEngine;
using XMUGFramework;

public abstract class ATrap : MonoBehaviour
{
    public float continueTime = 3f;
    public float stopTime = 5f;
    public float warmthReduce = 100f;
    [Header("ChainTraps")] public ATrap[] chainTraps;
    public float chainInterval = 0f;
    public bool hookedByOtherTrap = false;
    protected Collider playerCollider;
    protected bool working = true;
    public bool isImpacting = false;
    protected WaitForSeconds wfsCT;
    protected WaitForSeconds wfsST;

    protected virtual void Awake()
    {
        wfsCT = new WaitForSeconds(continueTime);
        wfsST = new WaitForSeconds(stopTime);
        GameCore.Get<EventSystem>().Register<PlayerDeathEvent>(param => { working = false; });
        GameCore.Get<EventSystem>().Register<PlayerReviveEvent>(param => { working = true; });

        foreach (var aTrap in chainTraps)
        {
            aTrap.hookedByOtherTrap = true;
        }
    }

    protected virtual void Start()
    {
        if (!hookedByOtherTrap)
        {
            StartCoroutine(Interval());
        }
    }

    protected virtual IEnumerator Interval()
    {
        while (gameObject.activeInHierarchy)
        {
            if (isImpacting)
            {
                _impact();
                ChainImpact(true);
                yield return wfsCT;
            }
            else
            {
                _deImpact();
                ChainImpact(false);
                yield return wfsST;
            }

            isImpacting = !isImpacting;
        }
    }

    protected void ChainImpact(bool flag)
    {
        if(flag)
        {
            StartCoroutine(Act());

            IEnumerator Act()
            {
                foreach (var aTrap in chainTraps)
                {
                    yield return new WaitForSeconds(chainInterval);
                    aTrap._impact();
                }
            }
        }
        else
        {
            StartCoroutine(DeAct());

            IEnumerator DeAct()
            {
                foreach (var aTrap in chainTraps)
                {
                    yield return new WaitForSeconds(chainInterval);
                    aTrap._deImpact();
                }
            }
        }
    }

    protected abstract void _impact();

    protected abstract void _deImpact();

    protected virtual void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && isImpacting && working &&
            GameCore.Get<PlayerModel>().invincibleTime.Value == 0)
        {
            playerCollider = other;
            GameCore.Get<EventSystem>().Trigger(new PlayerGetHitEvent()
            {
                warmthReduce = this.warmthReduce
            });
        }
    }
}