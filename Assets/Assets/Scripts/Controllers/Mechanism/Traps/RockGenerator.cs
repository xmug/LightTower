﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using Random = UnityEngine.Random;

namespace Assets.Scripts.Controllers.Mechanism.Traps
{
    public class RockGenerator : ATrap
    {
        public Transform baseMesh;
        public Transform generatePoint;
        public GameObject rockPrefab;
        private bool canWork;

        // Simple ObjectPool
        private Dictionary<string, List<GameObject>> pool = new Dictionary<string, List<GameObject>>();

        protected override void Awake()
        {
            base.Awake();
            if (generatePoint)
                canWork = true;
        }

        private GameObject GetObject(GameObject prefab)
        {
            GameObject rtObj = null;
            //查看是否有该名字所对应的子池，且子池中有对象
            if (pool.TryGetValue(gameObject.name, out var values))
            {
                if (values.Count > 0)
                {
                    rtObj = values.Find((o =>
                    {
                        if (!o.activeInHierarchy)
                            return true;
                        return false;
                    }));
                }

                if (!rtObj)
                {
                    rtObj = Instantiate(prefab, generatePoint.position, Quaternion.identity, generatePoint);
                    values.Add(rtObj);
                    rtObj.name = gameObject.name;
                }
            }
            else
            {
                //直接通过Instantiate生成
                rtObj = Instantiate(prefab, generatePoint.position, Quaternion.identity, generatePoint);
                List<GameObject> list = new List<GameObject>();
                list.Add(rtObj);
                pool.Add(gameObject.name, list);
                //修改名称(去掉Clone)
                rtObj.name = gameObject.name;
            }

            //设置为激活
            rtObj.SetActive(true);
            //返回
            return rtObj;
        }

        protected override void _impact()
        {
            if (!canWork) return;

            Vector3 pos = generatePoint.transform.position +
                          Random.insideUnitSphere * (baseMesh.localScale.z / 2 - 0.1f); // offset
            var obj = GetObject(rockPrefab);
            obj.transform.position = pos;
            obj.GetComponent<Rigidbody>().velocity = Vector3.zero;
        }

        protected override void _deImpact()
        {
            if (!canWork) return;
        }

        protected override void OnTriggerEnter(Collider other)
        {
            // override
        }
    }
}