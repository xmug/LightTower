using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Events;
using Assets.Scripts.Models;
using UnityEngine;
using XMUGFramework;

public class StabberController : ATrap
{
    private Animator anim;

    protected override void Awake()
    {
        base.Awake();
        anim = GetComponent<Animator>();
    }

    protected override void _impact()
    {
        anim.SetBool("isStabbing", true);
    }

    protected override void _deImpact()
    {
        anim.SetBool("isStabbing", false);
    }
}
