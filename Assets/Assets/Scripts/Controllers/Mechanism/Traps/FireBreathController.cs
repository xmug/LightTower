using System.Collections;
using Assets.Scripts.Events;
using Assets.Scripts.Models;
using UnityEngine;
using XMUGFramework;

public class FireBreathController : ATrap
{
    public GameObject fireBreath;

    protected override void _impact()
    {
        fireBreath.SetActive(true);
    }

    protected override void _deImpact()
    {
        fireBreath.SetActive(false);
    }
}