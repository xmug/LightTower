﻿using System;
using System.Collections;
using Assets.Scripts.Events;
using UnityEngine;
using XMUGFramework;

namespace Assets.Scripts.Controllers.Mechanism.Traps
{
    [RequireComponent(typeof(Collider))]
    [RequireComponent(typeof(Rigidbody))]
    public class Rock : AController
    {
        public float disableLimitTime = 15f;
        
        private void OnEnable()
        {
            StartCoroutine(DelayInactive());
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                GameCore.Get<EventSystem>().Trigger(new PlayerGetHitEvent()
                {
                    warmthReduce = 100f
                });
                this.gameObject.SetActive(false);
            }
        }

        IEnumerator DelayInactive()
        {
            for (float i = 0; i < disableLimitTime; i+= Time.fixedDeltaTime)
            {
                if (!this.gameObject.activeInHierarchy)
                {
                    yield break;
                }
                yield return null;
            }
            this.gameObject.SetActive(false);
        }
    }
}