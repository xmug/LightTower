﻿using System;
using TMPro;
using UnityEngine.UIElements;
using XMUGFramework;

namespace Assets.Scripts.Controllers
{
    public class AdapterManager : AManager
    {
        public AInteractable[] inputMechs;
        public AInteractable outputMech;
        private void Awake()
        {
            foreach (var inputMech in inputMechs)
            {
                inputMech.isActivated.OnValueChanged += b =>
                {
                    bool rt = true;
                    foreach (var mech in inputMechs)
                    {
                        rt &= mech.isActivated.Value;
                    }

                    if (rt)
                    {
                        outputMech.Activate();
                    }
                };
            }
        }
    }
}