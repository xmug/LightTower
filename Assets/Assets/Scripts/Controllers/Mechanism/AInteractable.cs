﻿using System;
using System.Collections;
using Assets.Scripts.Events;
using ParadoxNotion.Design;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Serialization;
using Random = System.Random;

namespace XMUGFramework
{
    public abstract class AInteractable : MonoBehaviour
    {
        [UnityEngine.Header("HUDInfo")] public string HUDInfo = "";

        [UnityEngine.Header("Setting")] [SerializeField] protected bool isActivatable;

        public virtual bool IsActivatable
        {
            get => isActivatable;
            set => isActivatable = value;
        }

        public BindableProperty<bool> isActivated = new BindableProperty<bool>()
        {
            Value = false
        };

        [UnityEngine.Header("Reset When Dead?")] public bool isResetWhenDead = true;
        
        public AInteractable[] chainMechs;
        public float chainMechInterval = 0.25f;
        public bool manullyTriggerable = true;
        protected Collider playerCollider;
        [UnityEngine.Header("CG Clip")] public PlayableAsset timeLineIndex;

        protected virtual void Awake()
        {
            foreach (var mech in chainMechs)
            {
                mech.manullyTriggerable = false;
            }

            if (chainMechs.Length > 0) manullyTriggerable = true;

            GameCore.Get<EventSystem>().Register<PlayerActivateEvent>((param =>
            {
                if (IsActivatable)
                {
                    if (!isActivated.Value)
                    {
                        if (!manullyTriggerable) return;
                        Activate();
                    }
                    else
                    {
                        if (!manullyTriggerable) return;
                        DeActivate();
                    }
                }
            }));

            GameCore.Get<EventSystem>().Register<PlayerDeathEvent>((param =>
            {
                if (isResetWhenDead)
                {
                    DeActivate();
                }
            }));
        }

        public void Activate()
        {
            _activate();
            if (IsActivatable)
            {
                GameCore.Get<EventSystem>().Trigger(new ObjectEvents.ObjectActivateCallBack()
                {
                    toActivate = true,
                    interactableObject = this
                });
            }

            isActivated.Value = true;


            if (timeLineIndex)
            {
                GameCore.Get<EventSystem>().Trigger(new PlayClipEvent(timeLineIndex));
            }

            StartCoroutine(Act());

            IEnumerator Act()
            {
                foreach (var mech in chainMechs)
                {
                    yield return new WaitForSeconds(chainMechInterval);
                    mech.Activate();
                }
            }
        }

        public void DeActivate()
        {
            _deActivate();
            if (IsActivatable)
            {
                GameCore.Get<EventSystem>().Trigger(new ObjectEvents.ObjectActivateCallBack()
                {
                    toActivate = false,
                    interactableObject = this
                });
            }

            isActivated.Value = false;


            StartCoroutine(DeAct());

            IEnumerator DeAct()
            {
                foreach (var mech in chainMechs)
                {
                    yield return new WaitForSeconds(chainMechInterval);
                    mech.DeActivate();
                }
            }
        }

        protected virtual void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                IsActivatable = true;
                playerCollider = other;
                GameCore.Get<EventSystem>().Trigger(new PlayerHUDUpdateEvent()
                {
                    info = HUDInfo
                });
            }
        }

        protected virtual void OnTriggerExit(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                IsActivatable = false;
                playerCollider = null;
                GameCore.Get<EventSystem>().Trigger(new PlayerHUDUpdateEvent()
                {
                    info = ""
                });
            }
        }

        /// <summary>
        /// Player only
        /// </summary>
        protected abstract void _activate();

        protected abstract void _deActivate();
    }
}