using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Events;
using Assets.Scripts.Models;
using UnityEditor;
using UnityEngine;
using UnityEngine.Serialization;
using XMUGFramework;

public class LightSpotTrigger : MonoBehaviour
{
    public float flickTime = 1f;
    private bool isFlickering = false;
    public bool isStatic = true;
    public GameObject pointLight;
    public LayerMask layerMask = 9;
    public float offsetY;
    public float originPosY;
    public Light spotLight;
    private float oriAngle;
    private float oriIntensity;

    private void Awake()
    {
        GameCore.Get<EventSystem>().Register<ObjectEvents.ObjectActivateCallBack>(param =>
        {
            switch (param.interactableObject)
            {
                case ObjectPushPull objectPushPull:
                    if (param.toActivate)
                    {
                        isFlickering = true;
                    }
                    else
                    {
                        isFlickering = false;
                    }
                    break;
            }
        });
    }

    private void Start()
    {
        spotLight = GetComponentInParent<Light>();
        if (!isStatic)
        {
            oriAngle = spotLight.innerSpotAngle;
            oriIntensity = spotLight.intensity;
            GameCore.Get<PlayerModel>().torchFuel.OnValueChanged += f =>
            {
                spotLight.innerSpotAngle = (1 + f / 100f) / 2 * oriAngle;
                spotLight.intensity = oriIntensity * f / 100f;
            };
        }

        StartCoroutine(Flicker());
    }

    private void Update()
    {
        transform.rotation = Quaternion.Euler(90, 0, 0);
        if (pointLight)
        {
            float r = offsetY / 2;
            float diff = Mathf.Abs(originPosY - transform.parent.position.y);
            float moveOffset = 2 * Mathf.Sqrt(r * r - diff * diff);
            var pos = new Vector3(transform.parent.position.x, originPosY, transform.parent.position.z) +
                      Vector3.up * (r * r > diff * diff ? moveOffset : 9999f);
            transform.position = pos;
        }

        if (Physics.Raycast(transform.parent.position, Vector3.up * -1, out var hitInfo, 1000f, layerMask.value))
        {
            originPosY = hitInfo.point.y;
        }
    }

    IEnumerator Flicker()
    {
        while (true)
        {
            if(isFlickering)
            {
                spotLight.enabled = false;
                yield return new WaitForSeconds(flickTime);
                spotLight.enabled = true;
                yield return new WaitForSeconds(flickTime);
            }

            yield return null;
        }
    }
}