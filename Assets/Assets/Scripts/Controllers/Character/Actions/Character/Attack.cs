using Assets.Scripts.Controllers;

namespace RPGCharacterAnims.Actions
{
    public class AttackContext
    {
        public string type;
        public int side;
        public int number;

        public AttackContext(string type, int side, int number = -1)
        {
            this.type = type;
            this.side = side;
            this.number = number;
        }

        public AttackContext(string type, string side, int number = -1)
        {
            this.type = type;
            this.number = number;
            switch (side.ToLower())
            {
                case "none":
                    this.side = (int) AttackSide.None;
                    break;
                case "left":
                    this.side = (int) AttackSide.Left;
                    break;
                case "right":
                    this.side = (int) AttackSide.Right;
                    break;
                case "dual":
                    this.side = (int) AttackSide.Dual;
                    break;
            }
        }
    }

    public class Attack : BaseActionHandler<AttackContext>
    {
        public override bool CanStartAction(PlayerController controller)
        {
            return !controller.isRelaxed && !active && controller.canAction;
        }

        public override bool CanEndAction(PlayerController controller)
        {
            return active;
        }

        protected override void _StartAction(PlayerController controller, AttackContext context)
        {
            int attackSide = 0;
            int attackNumber = context.number;
            int weaponNumber = 1;
            float duration = 0f;

            duration = AnimationData.AttackDuration(attackSide, weaponNumber, attackNumber);

            controller.RunningAttack(
                attackSide,
                false,
                true,
                false,
                false
            );
            EndAction(controller);
        }

        protected override void _EndAction(PlayerController controller)
        {
        }
    }
}