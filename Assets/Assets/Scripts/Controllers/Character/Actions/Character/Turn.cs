// // Turn Left - 1
// // Turn Right - 2
//
// using Assets.Scripts.Controllers;
//
// namespace RPGCharacterAnims.Actions
// {
//     public class Turn : InstantActionHandler<int>
//     {
//         public override bool CanStartAction(PlayerController controller)
//         {
// 			return controller.canMove;
//         }
//
//         protected override void _StartAction(PlayerController controller, int context)
//         {
//             controller.Turn(context);
//         }
//     }
// }