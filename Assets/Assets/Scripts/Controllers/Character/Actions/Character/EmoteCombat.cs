using Assets.Scripts.Controllers;

namespace RPGCharacterAnims.Actions
{
    public class EmoteCombat : InstantActionHandler<string>
    {
        public override bool CanStartAction(PlayerController controller)
        {
            return controller.canAction;
        }

        protected override void _StartAction(PlayerController controller, string context)
        {
            switch (context) {
                case "Pickup":
                    controller.Pickup();
                    break;
                case "Activate":
                    controller.Activate();
                    break;
                case "Boost":
                    controller.Boost();
                    break;
                case "Dash":
                    controller.Dash();
                    break;
            }
        }
    }
}