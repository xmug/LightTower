// // Dodge Left - 1
// // Dodge Right - 2
//
// using Assets.Scripts.Controllers;
//
// namespace RPGCharacterAnims.Actions
// {
//     public class Dodge : InstantActionHandler<int>
//     {
//         public override bool CanStartAction(PlayerController controller)
//         {
// 			return controller.canAction && !controller.IsActive("Relax");
// 		}
//
//         protected override void _StartAction(PlayerController controller, int context)
//         {
//             controller.GetAngry();
//             controller.Dodge(context);
//         }
//     }
// }