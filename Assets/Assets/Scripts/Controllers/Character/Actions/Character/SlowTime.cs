using Assets.Scripts.Controllers;
using UnityEngine;

namespace RPGCharacterAnims.Actions
{
    public class SlowTime : BaseActionHandler<float>
    {
        public override bool CanStartAction(Assets.Scripts.Controllers.PlayerController controller)
        {
            return !active;
        }

        public override bool CanEndAction(Assets.Scripts.Controllers.PlayerController controller)
        {
            return active;
        }

        protected override void _StartAction(Assets.Scripts.Controllers.PlayerController controller, float context)
        {
            Time.timeScale = context;
        }

        protected override void _EndAction(Assets.Scripts.Controllers.PlayerController controller)
        {
            Time.timeScale = 1f;
        }
    }
}