using Assets.Scripts.Controllers;

namespace RPGCharacterAnims.Actions
{
    public class Fall : MovementActionHandler<EmptyContext>
    {
        public Fall(PlayerController movement) : base(movement)
        {
        }

        public override bool CanStartAction(Assets.Scripts.Controllers.PlayerController controller)
        {
            return !controller.maintainingGround;
        }

        protected override void _StartAction(Assets.Scripts.Controllers.PlayerController controller, EmptyContext context)
        {
            movement.currentState = PlayerState.Fall;
        }

        public override bool IsActive()
        {
            return movement.currentState != null && (PlayerState)movement.currentState == PlayerState.Fall;
        }
    }
}