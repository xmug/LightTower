using Assets.Scripts.Controllers;

namespace RPGCharacterAnims.Actions
{
    public class PushPull : MovementActionHandler<EmptyContext>
    {
        public PushPull(PlayerController movement) : base(movement)
        {
        }

        public override bool CanStartAction(Assets.Scripts.Controllers.PlayerController controller)
        {
            return (movement.canJump || movement.canDoubleJump) &&
                   controller.maintainingGround &&
                   controller.canAction;
        }

        protected override void _StartAction(Assets.Scripts.Controllers.PlayerController controller, EmptyContext context)
        {
            movement.currentState = PlayerState.PushPull;
        }

        public override bool IsActive()
        {
            return movement.currentState != null && (PlayerState)movement.currentState == PlayerState.PushPull;
        }
    }
}