using System.Collections;
using Assets.Scripts.Controllers;
using UnityEngine;

namespace RPGCharacterAnims.Actions
{
    public class Idle : MovementActionHandler<EmptyContext>
    {
        IEnumerator randomIdleCoroutine;

        public Idle(PlayerController movement) : base(movement)
        {
        }

        public override bool CanStartAction(Assets.Scripts.Controllers.PlayerController controller)
        {
            if (controller.isMoving) { return controller.MoveInput.magnitude < 0.2f; }
            return controller.maintainingGround || controller.acquiringGround;
        }

        protected override void _StartAction(Assets.Scripts.Controllers.PlayerController controller, EmptyContext context)
        {
            movement.currentState = PlayerState.Idle;
            if (randomIdleCoroutine != null) { controller.StopCoroutine(randomIdleCoroutine); }
            StartRandomIdleCountdown(controller);
        }

        public override bool IsActive()
        {
            return movement.currentState != null && (PlayerState)movement.currentState == PlayerState.Idle;
        }

        private void StartRandomIdleCountdown(Assets.Scripts.Controllers.PlayerController controller)
        {
            randomIdleCoroutine = RandomIdle(controller);
            controller.StartCoroutine(randomIdleCoroutine);
        }

        private IEnumerator RandomIdle(Assets.Scripts.Controllers.PlayerController controller)
        {
            float waitTime = Random.Range(15f, 25f);
            yield return new WaitForSeconds(waitTime);

            // If we're not still idling, stop here.
            if (!IsActive()) {
                randomIdleCoroutine = null;
                yield break;
            }

            if (controller.canMove) { controller.RandomIdle(); }

            StartRandomIdleCountdown(controller);
        }
    }
}