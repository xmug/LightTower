using Assets.Scripts.Controllers;
using UnityEngine;

namespace RPGCharacterAnims.Actions
{
    public class Move : MovementActionHandler<EmptyContext>
    {
        public Move(PlayerController movement) : base(movement)
        {
        }

        public override bool CanStartAction(Assets.Scripts.Controllers.PlayerController controller)
        {
            return controller.canMove &&
                   controller.MoveInput != Vector3.zero &&
                   controller.maintainingGround;
        }

        protected override void _StartAction(Assets.Scripts.Controllers.PlayerController controller, EmptyContext context)
        {
            movement.currentState = PlayerState.Move;
        }

        public override bool IsActive()
        {
            return movement.currentState != null && (PlayerState)movement.currentState == PlayerState.Move;
        }
    }
}