using Assets.Scripts.Controllers;

namespace RPGCharacterAnims.Actions
{
    public class DoubleJump : MovementActionHandler<EmptyContext>
    {
        public DoubleJump(PlayerController movement) : base(movement)
        {
        }

        public override bool CanStartAction(Assets.Scripts.Controllers.PlayerController controller)
        {
            return controller.isFalling && movement.canDoubleJump;
        }

        protected override void _StartAction(Assets.Scripts.Controllers.PlayerController controller, EmptyContext context)
        {
            movement.currentState = PlayerState.DoubleJump;
        }

        public override bool IsActive()
        {
            return movement.currentState != null && (PlayerState)movement.currentState == PlayerState.DoubleJump;
        }
    }
}