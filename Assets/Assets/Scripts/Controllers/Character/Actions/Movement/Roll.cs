// Roll Forward - 1
// Roll Right - 2
// Roll Backward - 3
// Roll Left - 4

using Assets.Scripts.Controllers;

namespace RPGCharacterAnims.Actions
{
    public class Roll : MovementActionHandler<int>
    {
        public Roll(PlayerController movement) : base(movement)
        {
        }

        public override bool CanStartAction(Assets.Scripts.Controllers.PlayerController controller)
        {
            return controller.canAction &&
                   (PlayerState) movement.currentState != PlayerState.Jump && !controller.isFalling;
        }

        protected override void _StartAction(Assets.Scripts.Controllers.PlayerController controller, int context)
        {
            controller.Roll(context);
            movement.currentState = PlayerState.Roll;
		}

        public override bool IsActive()
        {
            return movement.currentState != null && (PlayerState)movement.currentState == PlayerState.Roll;
        }
    }
}