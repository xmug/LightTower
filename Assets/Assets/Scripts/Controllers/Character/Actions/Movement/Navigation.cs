// using Assets.Scripts.Controllers;
// using UnityEngine;
//
// namespace RPGCharacterAnims.Actions
// {
//     public class Navigation : BaseActionHandler<Vector3>
//     {
//         RPGCharacterNavigationController navigation;
//
//         public Navigation(RPGCharacterNavigationController navigation)
//         {
//             this.navigation = navigation;
//         }
//
//         public override bool CanStartAction(Assets.Scripts.Controllers.PlayerController controller)
//         {
//             return navigation != null && controller.canMove;
//         }
//
//         public override bool CanEndAction(Assets.Scripts.Controllers.PlayerController controller)
//         {
//             return navigation != null && navigation.isNavigating;
//         }
//
//         protected override void _StartAction(Assets.Scripts.Controllers.PlayerController controller, Vector3 context)
//         {
//             navigation.MeshNavToPoint(context);
//         }
//
//         public override bool IsActive()
//         {
//             return navigation.isNavigating;
//         }
//
//         protected override void _EndAction(Assets.Scripts.Controllers.PlayerController controller)
//         {
//             navigation.StopNavigating();
//         }
//     }
// }