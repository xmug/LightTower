using Assets.Scripts.Controllers;

namespace RPGCharacterAnims.Actions
{
    public class DiveRoll : MovementActionHandler<int>
    {
        public DiveRoll(PlayerController movement) : base(movement)
        {
        }

        public override bool CanStartAction(Assets.Scripts.Controllers.PlayerController controller)
        {
            return controller.canAction;
        }

        protected override void _StartAction(Assets.Scripts.Controllers.PlayerController controller, int context)
        {
            controller.DiveRoll(context);
            movement.currentState = PlayerState.DiveRoll;
		}

        public override bool IsActive()
        {
            return movement.currentState != null && (PlayerState)movement.currentState == PlayerState.DiveRoll;
        }
    }
}