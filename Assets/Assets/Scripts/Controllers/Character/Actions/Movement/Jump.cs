using Assets.Scripts.Controllers;

namespace RPGCharacterAnims.Actions
{
    public class Jump : MovementActionHandler<EmptyContext>
    {
        public Jump(PlayerController movement) : base(movement)
        {
        }

        public override bool CanStartAction(Assets.Scripts.Controllers.PlayerController controller)
        {
            return (movement.canJump || movement.canDoubleJump) &&
                   controller.maintainingGround &&
				controller.canAction;
        }

        protected override void _StartAction(Assets.Scripts.Controllers.PlayerController controller, EmptyContext context)
        {
            // controller.GetAngry();
            movement.currentState = PlayerState.Jump;
        }

        public override bool IsActive()
        {
            return movement.currentState != null && (PlayerState)movement.currentState == PlayerState.Jump;
        }
    }
}