using Assets.Scripts.Controllers;

namespace RPGCharacterAnims.Actions
{
    public class Null : InstantActionHandler<EmptyContext>
    {
        public override bool CanStartAction(Assets.Scripts.Controllers.PlayerController controller)
        {
            return false;
        }

        protected override void _StartAction(Assets.Scripts.Controllers.PlayerController controller, EmptyContext context)
        {
        }
    }
}