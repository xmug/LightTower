using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Events;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using XMUGFramework;

namespace Assets.Scripts.Controllers
{
    public class DirectorController : MonoBehaviour
    {
        private PlayableDirector director;
        public TimelineAsset[] timelineAssets;
        private EventSystem eSys;

        private void Awake()
        {
            eSys = GameCore.Get<EventSystem>();
            director = GetComponent<PlayableDirector>();

            eSys.Register<PlayClipEvent>(param =>
            {
                director.playableAsset = param.clipIndex;
                director.Play();
                eSys.Trigger(new PlayerInputLockEvent((float) director.playableAsset.duration));
            });
        }
    }
}