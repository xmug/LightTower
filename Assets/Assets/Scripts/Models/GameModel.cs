﻿using UnityEngine;
using XMUGFramework;

namespace Assets.Scripts.Models
{
    public enum GameState
    {
        PreGame,
        Start,
        Pause,
        End,
        PostGame
    }
    
    public class GameModel : AModel
    {
        public BindableProperty<GameState> gameState = new BindableProperty<GameState>();
        [ExposedField] public BindableProperty<Vector3> savePointPos = new BindableProperty<Vector3>();

        public override void Init()
        {
            gameState.Value = GameState.PreGame;
            var stg = GameCore.Get<PlayerPrefsStorage>();
            
            savePointPos.Value = (Vector3) stg.GetValue("LTP_SAVE", Vector3.zero);
            
            savePointPos.OnValueChanged += i =>
            {
                stg.SetValue("LTP_SAVE", i);
            };
        }
    }
}