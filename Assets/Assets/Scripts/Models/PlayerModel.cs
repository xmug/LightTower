﻿using System;
using Assets.Scripts.Controllers;
using Assets.Scripts.Events;
using UnityEngine;
using XMUGFramework;

namespace Assets.Scripts.Models
{
    [Serializable]
    public class PlayerInput
    {
        [ExposedField] public Vector3 input;
        [ExposedProperty] public Vector2 property
        {
            get;
            set;
        }
    }

    public class PlayerModel : AModel
    {
        public PlayerController playerController;
        [ExposedField] public BindableProperty<float> warmth = new BindableProperty<float>();
        [ExposedField] public BindableProperty<float> torchFuel = new BindableProperty<float>();
        [ExposedField] public BindableProperty<Vector3> currentVelocity = new BindableProperty<Vector3>();
        [ExposedField] public float reviveTime = 3f;
        [ExposedField] public BindableProperty<float> invincibleTime = new BindableProperty<float>();
        public BindableProperty<int> keyNumber = new BindableProperty<int>();

        public BindableProperty<int> goldCoinNum = new BindableProperty<int>();

        public override void Init()
        {
            // Init
            warmth.Value = 100f;
            torchFuel.Value = 100f;
            
            var stg = GameCore.Get<PlayerPrefsStorage>();

            GameCore.Get<EventSystem>().Register<PlayerGetHitEvent>((param =>
            {
                warmth.Value -= param.warmthReduce;
                invincibleTime.Value = 1f;
            }));
            
            GameCore.Get<EventSystem>().Register<PlayerReviveEvent>((param =>
            {
                warmth.Value = 100f;
                torchFuel.Value = 100f;
            }));
            
            
        }
    }
}