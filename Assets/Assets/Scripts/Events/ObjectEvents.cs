﻿using XMUGFramework;

namespace Assets.Scripts.Events
{
    public class ObjectEvents
    {
        public struct ObjectActivateCallBack : IEvent
        {
            public bool toActivate;
            public AInteractable interactableObject;


            public ObjectActivateCallBack(bool toActivate, AInteractable interactableObject)
            {
                this.toActivate = toActivate;
                this.interactableObject = interactableObject;
            }
        }
    }
}