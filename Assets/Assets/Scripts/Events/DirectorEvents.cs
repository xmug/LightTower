﻿using System.Collections;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Playables;
using XMUGFramework;

namespace Assets.Scripts.Events
{
    public struct PlayClipEvent : IEvent
    {
        public PlayableAsset clipIndex;


        public PlayClipEvent([NotNull] PlayableAsset clipIndex)
        {
            this.clipIndex = clipIndex;
        }
    }
}