﻿using System.Collections;
using UnityEngine;
using XMUGFramework;

namespace Assets.Scripts.Events
{
    public struct PlayerDeathEvent : IEvent
    {
    }
    
    public struct PlayerReviveEvent : IEvent
    {
    }

    public struct PlayerGetHitEvent : IEvent
    {
        public float warmthReduce;
    }

    public struct PlayerHUDUpdateEvent : IEvent
    {
        public string info;
    }

    public struct PlayerActivateEvent : IEvent
    {
        
    }
    
    public struct PlayerAttackGhostEvent : IEvent
    {
        
    }

    public struct PlayerInputLockEvent : IEvent
    {
        public float time;
        
        public PlayerInputLockEvent(float time)
        {
            this.time = time;
        }
    }
    
}