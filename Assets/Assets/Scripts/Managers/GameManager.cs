﻿using System;
using Assets.Scripts.Events;
using Assets.Scripts.Models;
using UnityEngine.Playables;
using XMUGFramework;

namespace Assets.Scripts.Managers
{
    public class GameManager : AManager
    {
        public PlayableAsset levelStartCG;
        private GameModel gModel;
        private DirectorModel dModel;
        private EventSystem eSys;

        private void Awake()
        {
            gModel = GameCore.Get<GameModel>();
            eSys = GameCore.Get<EventSystem>();
        }

        private void Start()
        {
            if(levelStartCG)
            {
                eSys.Trigger(new PlayClipEvent(levelStartCG));
            }
        }
    }
}