﻿using System;
using Assets.Scripts.Models;
using UnityEngine.UI;
using XMUGFramework;

namespace Assets.Scripts.Managers
{
    public class UIManager : AManager
    {
        public Slider warmthBar;
        public Slider torchFuelBar;

        private EventSystem eSys;
        private PlayerModel playerModel;

        private void Awake()
        {
            eSys = GameCore.Get<EventSystem>();
            playerModel = GameCore.Get<PlayerModel>();

            playerModel.warmth.OnValueChanged += f =>
            {
                warmthBar.value = f / 100f;
            };
            
            playerModel.torchFuel.OnValueChanged += f =>
            {
                torchFuelBar.value = f / 100f;
            };
        }
    }
}