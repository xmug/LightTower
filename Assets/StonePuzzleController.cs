using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XMUGFramework;

public class StonePuzzleController : AInteractable
{
    public StonePuzzleController[] hookedStones;
    
    protected override void _activate()
    {
        
    }

    protected override void _deActivate()
    {
        
    }
    
    protected override void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            isActivated.Value = true;
        }
    }

    protected override void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            isActivated.Value = false;
        }
    }
}
