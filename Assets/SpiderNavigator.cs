using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class SpiderNavigator : MonoBehaviour
{
    public float speed;
    public LayerMask mask;
    private NavMeshAgent agent;
    private Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponentInChildren<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0) && Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out var hitInfo, 1000f, mask))
        {
            agent.SetDestination(hitInfo.point);
            agent.speed = speed;
        }

        if (agent.speed != 0)
        {
            // anim.SetFloat("locomotion", agent.velocity.magnitude);
        }
    }
}
